package commons;

public class JsonResult<T> {
	private T data;
	private String message;
	private String status;
	
	public JsonResult( String message, String status) {
		this.message = message;
		this.status = status;
	}
	
	public JsonResult(T data, String message, String status) {
		this.data = data;
		this.message = message;
		this.status = status;
	}
	
	public Object getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
