package commons;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

public class JsonResultWriter {
	public static void writer(HttpServletResponse response,Object obj) throws IOException {
		Gson gson = new Gson();
		String json = gson.toJson(obj);
		response.setContentType("application/json");
		response.setHeader("Content-Type", "text/html");
		response.getWriter().print(json);
		response.getWriter().close();
	}
	
	
	
}
