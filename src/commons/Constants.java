package commons;

public class Constants {
	public static  final String STATUS_NOTFOUND="404";
	public static final String STATUS_SUCCESS="200";
	public static final String STATUS_EX = "500";
	public static final String OPERATION = "/industrySMS/sendSMS";
	public static String ACCOUNTSID = Config.ACCOUNT_SID;
}
