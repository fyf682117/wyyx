package com.wyyx.dao;

import java.util.HashMap;
import java.util.List;

import com.wyyx.model.GoodsOrder;

import utils.SqlHelper;

/**
 * 购物车商品DAO操作
 * 
 * @author zzd19
 *
 */
public class GoodsOrderDaoImpl {
	/**
	 * 查询所有订单商品中间表
	 * 
	 * @return
	 */
	public List<GoodsOrder> selectAll() {
		String sql = "select * from goods_order";
		List<HashMap<String, Object>> list = SqlHelper.select1(sql);
		List<GoodsOrder> gos = utils.setAttrUtil.setGoodsOrder(list);
		return gos;
	}
	/**
	 * 根据订单商品ID查询所有订单商品中间表
	 * 
	 * @return
	 */
	public GoodsOrder selectId(String goId) {
		String sql = "select * from goods_order where go_id=?";
		List<HashMap<String, Object>> list = SqlHelper.select1(sql, goId);
		List<GoodsOrder> gos = utils.setAttrUtil.setGoodsOrder(list);
		return gos.get(0);
	}
	/**
	 * 根据订单商品的商品ID查询所有订单商品中间表
	 * 
	 * @return
	 */
	public List<GoodsOrder> selectOrder(String goOrderId) {
		String sql = "select * from goods_order where go_order_id=?";
		List<HashMap<String, Object>> list = SqlHelper.select1(sql, goOrderId);
		List<GoodsOrder> gos = utils.setAttrUtil.setGoodsOrder(list);
		return gos;
	}
	/**
	 * 创建订单商品中间表
	 * 
	 * @param goOrderId
	 * @param goGoodsId
	 * @param goGoodsCount
	 * @return
	 */
	public int addGO(String goOrderId, String goGoodsId, String goGoodsCount) {
		String sql = "INSERT INTO goods_order(go_id,go_order_id,go_goods_id,go_goods_count)VALUES(uuid(),?,?,?)";
		int i = SqlHelper.update(sql, goOrderId, goGoodsId, goGoodsCount);
		return i;
	}
}
