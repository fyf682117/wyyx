package com.wyyx.dao;

import java.util.HashMap;
import java.util.List;

import com.wyyx.model.User;

import utils.SqlHelper;
/**
 * 用户表的DAO层代码
 * 
 * @author zzd19
 *
 */
public class UserDaoImpl {
	/**
	 * 查询所有用户dao层
	 * 
	 * @param userId
	 * @return
	 */
	public List<User> userSelect(){
		String sql="select * from user";
		List<HashMap<String,Object>> list = SqlHelper.select1(sql);
		List<User> users = utils.setAttrUtil.setUser(list);
		SqlHelper.close();
		return users;
	}
	/**
	 * 查询ID用户dao层
	 * 
	 * @param userId
	 * @return
	 */
	public User userSelectId(String userId){
		String sql="select * from user where user_id=?";
		List<HashMap<String,Object>> list = SqlHelper.select1(sql, userId);
		List<User> users = utils.setAttrUtil.setUser(list);
		SqlHelper.close();
		return users.get(0);
	}
	/**
	 * 查询状态用户dao层
	 * 
	 * @param userId
	 * @return
	 */
	public List<User> userSelectStatus(String userStatus){
		String sql="select * from user where user_status=?";
		List<HashMap<String,Object>> list = SqlHelper.select1(sql, userStatus);
		List<User> users = utils.setAttrUtil.setUser(list);
		SqlHelper.close();
		return users;
	}
	/**
	 * 用户登录dao层
	 * 手机号登录
	 * 
	 * @param userTelphone
	 * @return
	 */
	public User userLoginSelect(String userTelphone){
		String sql="select * from user where user_telphone=? and user_status=1";
		List<HashMap<String,Object>> list = SqlHelper.select1(sql, userTelphone);
		List<User> users = utils.setAttrUtil.setUser(list);
		SqlHelper.close();
		return users.get(0);
	}
	/**
	 * 用户登录dao层
	 * 邮箱登录
	 * 
	 * @param userEmail
	 * @param userPassword
	 * @return
	 */
	public User userLoginSelect(String userEmail, String userPassword){
		String sql="select * from user where user_email=? and user_password=? and user_status=1";
		List<HashMap<String,Object>> list = SqlHelper.select1(sql, userEmail, userPassword );
		List<User> users = utils.setAttrUtil.setUser(list);
		SqlHelper.close();
		return users.get(0);
	}
	/**
	 * 用户注册dao层
	 * 
	 * @param userUserName
	 * @param userTelphone
	 * @param userEmail
	 * @param userPassword
	 */
	public int registerINSERT(String userUserName,String userTelphone,String userEmail,String userPassword) {
		String sql = "INSERT INTO user(user_id,user_user_name,user_telphone,user_email,user_password)VALUES(uuid(),?,?,?,?)";
		int i = SqlHelper.update(sql,userUserName,userTelphone,userEmail,userPassword);
		SqlHelper.close();
		return i;
	}
	/**
	 * 更新个人信息
	 * 
	 * @param userId
	 * @param userUserName
	 * @param userTelphone
	 * @param userEmail
	 * @param userAddress
	 * @param userSex
	 * @param userBirthday
	 */
	public int update(String userId, String userUserName, String userTelphone, String userEmail, String userAddress, String userSex, String userBirthday ) {
		String sql = "UPDATE user SET user_user_name=?, user_telphone=?, user_email=?, user_address=?, user_sex=?, user_birthday=? WHERE user_id = ?";
		int i = SqlHelper.update(sql, userUserName, userTelphone, userEmail, userAddress, userSex, userBirthday, userId);
		SqlHelper.close();
		return i;
	}
	/**
	 * 实名认证
	 * 
	 * @param userId
	 * @param userRealName
	 * @param userIdNumber
	 * @return
	 */
	public int updateAuthentication(String userId, String userRealName, String userIdNumber ) {
		String sql = "UPDATE user SET user_real_name=?, user_id_number=? WHERE user_id = ?";
		int i = SqlHelper.update(sql, userRealName, userIdNumber, userId);
		SqlHelper.close();
		return i;
	}
	/**
	 * 更改密码
	 * 
	 * @param userId
	 * @param userPassword
	 * @return
	 */
	public int updatePassword(String userId, String userPassword ) {
		String sql = "UPDATE user SET user_password=? WHERE user_id = ?";
		int i = SqlHelper.update(sql, userPassword, userId);
		SqlHelper.close();
		return i;
	}
	/**
	 * 更新积分
	 * 
	 * @param userId
	 * @param userScore
	 * @return
	 */
	public int updateScore(String userId, String userScore ) {
		String sql = "UPDATE user SET user_score=? WHERE user_id = ?";
		int i = SqlHelper.update(sql, userScore, userId);
		SqlHelper.close();
		return i;
	}
	/**
	 * 更新状态
	 * 
	 * @param userId
	 * @param userStatus
	 * @return
	 */
	public int updateStatus(String userId, String userStatus ) {
		String sql = "UPDATE user SET user_status=? WHERE user_id = ?";
		int i = SqlHelper.update(sql, userStatus, userId);
		SqlHelper.close();
		return i;
	}
	/**
	 * 更换头像
	 * 
	 * @param userId
	 * @param userImage
	 * @return
	 */
	public int updateImage(String userId, String userImage ) {
		String sql = "UPDATE user SET user_image=? WHERE user_id = ?";
		int i = SqlHelper.update(sql, userImage, userId);
		SqlHelper.close();
		return i;
	}
}
