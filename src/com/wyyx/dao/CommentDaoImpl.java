package com.wyyx.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.wyyx.model.Comment;

import utils.SqlHelper;
import utils.StringUtils;
import utils.setAttrUtil;

public class CommentDaoImpl {
	/**
	 * 查看用户所有评论
	 * @param commentUserId
	 * @return
	 */
	public List<Comment> selectAll(String commentUserId){
		String sql ="select * from comment where comment_user_id =?";//通过用户id查看用户的所有评论
		List<HashMap<String,Object>>  list = SqlHelper.select1(sql, commentUserId);
		List<Comment> commentList =utils.setAttrUtil.setComment(list);
		
		SqlHelper.close();
		return commentList;
	}
	/**
	 * 查看商品的所有评论
	 * @param commentGoodsId
	 * @return
	 */
	public List<Comment> selectAll2(String commentGoodsId){
		String sql ="select * from comment where comment_goods_id =?";
		List<HashMap<String,Object>>  list2 = SqlHelper.select1(sql, commentGoodsId);
		List<Comment> commentList2 =utils.setAttrUtil.setComment(list2);//
		SqlHelper.close();
		return commentList2;
	}
	/**
	 * 添加评论
	 * @param commentGoodsId
	 * @param commentUserId
	 * @param commentContent
	 * @return
	 */
	public  int insert(String commentGoodsId,String commentUserId,String commentContent){
		String sql ="insert  into comment(comment_id,comment_goods_id,comment_user_id,comment_content)  values (uuid(),?,?,?)";
		int i=SqlHelper.update(sql,commentGoodsId,commentUserId,commentContent);
		SqlHelper.close();
		return i;
	}
	/**
	 * 删除评论
	 * @param commentid
	 * @return
	 */
	public int delect(String commentStatus ,String commentId){
		String sql ="update comment set comment_status=?  where comment_id =?";
		int i =SqlHelper.update(sql,commentStatus, commentId);//**方法里面要有(sql,params)
		SqlHelper.close();
		return i;
	}
	 
}
