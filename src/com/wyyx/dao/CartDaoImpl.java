package com.wyyx.dao;

import java.util.HashMap;
import java.util.List;

import com.wyyx.model.Cart;
import com.wyyx.vo.CartGoods;

import utils.SqlHelper;

/**
 * 购物车DAO操作
 * 
 * @author zzd19
 *
 */
public class CartDaoImpl {
	/**
	 * 查看所有用户的购物车
	 * 
	 * @return
	 */
	public List<CartGoods> cartSelectAll(){
		String sql = "select * from cart LEFT JOIN goods ON cart_goods_id=goods_id";//cart_goods_id=goods_id  要对应
		List<HashMap<String, Object>> list = SqlHelper.select1(sql);
		List<CartGoods> carts = utils.setAttrUtil.setCartGoods(list);
		SqlHelper.close();
		return carts;
	}
	/**
	 * 根据购物车ID查询
	 * 
	 * @param cardId
	 * @return
	 */
	public CartGoods cartSelectId(String cartId) {
		String sql = "select * from cart LEFT JOIN goods ON cart_goods_id=goods_id where cart_id=?";
		List<HashMap<String, Object>> list = SqlHelper.select1(sql, cartId);
		List<CartGoods> carts = utils.setAttrUtil.setCartGoods(list);
		SqlHelper.close();
		return carts.get(0);
	}
	/**
	 * 查询某用户下的购物车
	 * 
	 * @param cardUserId
	 * @return
	 */
	public List<CartGoods> cartSelectUser(String cartUserId) {
		String sql = "select * from cart LEFT JOIN goods ON cart_goods_id=goods_id where cart_user_id=? and cart_status=1";
		List<HashMap<String, Object>> list = SqlHelper.select1(sql, cartUserId);
		List<CartGoods> carts = utils.setAttrUtil.setCartGoods(list);
		SqlHelper.close();
		return carts;
	}
	/**
	 * 查询用户下的购物车中某件商品
	 * 
	 * @param cartUserId
	 * @param cartGoodsId
	 * @return
	 */
	public List<CartGoods> cartSelectGoods(String cartUserId, String cartGoodsId) {
		String sql = "select * from cart LEFT JOIN goods ON cart_goods_id=goods_id where cart_user_id=? and cart_goods_id=?";
		List<HashMap<String, Object>> list = SqlHelper.select1(sql, cartUserId, cartGoodsId);
		List<CartGoods> carts = utils.setAttrUtil.setCartGoods(list);
		SqlHelper.close();
		return carts;
	}
	/**
	 * 查询用户下的各种状态的购物车
	 * 
	 * @param cartUserId
	 * @param cartStatus
	 * @return
	 */
	public List<Cart> cartSelectStatus(String cartUserId, String cartStatus) {
		String sql = "select * from cart LEFT JOIN goods ON cart_goods_id=goods_id where cart_user_id=? and cart_status=?";
		List<HashMap<String, Object>> list = SqlHelper.select1(sql, cartUserId, cartStatus);
		List<Cart> carts = utils.setAttrUtil.setCart(list);
		SqlHelper.close();
		return carts;
	}
	/**
	 * 添加到购物车
	 * 
	 * @param cartUserId
	 * @param cartGoodsId
	 * @param cartGoodsNumber
	 * @return
	 */
	public int cartAdd(String cartUserId, String cartGoodsId, String cartGoodsNumber, String cartPrice) {
		String sql="INSERT INTO cart(cart_id,cart_user_id,cart_goods_id,cart_goods_number,cart_price=?)VALUES(uuid(),?,?,?,?)";
		int i = SqlHelper.update(sql, cartUserId, cartGoodsId, cartGoodsNumber, cartPrice);
		SqlHelper.close();
		return i;
	}
	/**
	 * 更新购物车商品数量and价钱
	 * 
	 * @param cartId
	 * @param cartGoodsNumber
	 * @return
	 */
	public int cartNumber(String cartId, String cartGoodsNumber, String cartPrice) {
		String sql = "UPDATE cart SET cart_goods_number=?,cart_price=? WHERE cart_id = ?";
		int i = SqlHelper.update(sql, cartGoodsNumber, cartPrice, cartId);
		SqlHelper.close();
		return i;
	}
	/**
	 * 更新购物车中商品状态
	 * 
	 * @param cartId
	 * @param cartStatus
	 * @return
	 */
	public int cartDelete(String cartId) {
		String sql = "UPDATE cart SET cart_status=0 WHERE cart_id = ?";
		 SqlHelper.update(sql, cartId);
		SqlHelper.close();
		return 0;
	}
}
