package com.wyyx.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.wyyx.model.Favorite;
import com.wyyx.vo.FavoriteGoods;

import utils.SqlHelper;
import utils.StringUtils;
public class FavoriteDaoImpl {
/**
 * 收藏夹查看个人的所有收藏夹商品信息
 * @return
 */
	public List<FavoriteGoods> selectAll(String favoriteUserId){
		String sql="select * from favorite left join goods ON favorite_goods_id= goods_id where favorite_user_id=?";//通过登录的用户id查找所有收藏
		List<HashMap<String,Object>> list =SqlHelper.select1(sql,favoriteUserId);
		List<FavoriteGoods> favoriteList =utils.setAttrUtil.setFavoriteGoods(list);
		
		SqlHelper.close();
		return favoriteList;
	}
	/**
	 * 收藏夹查看用户 个人的收藏夹内的单个商品信息
	 * @param favoriteGoodsId
	 * @return
	 */
	public FavoriteGoods selectone(String favoriteUserId,String favoriteGoodsId){
		//通过用户id和商品id查询所有的商品
		String sql="select * from favorite left join goods ON favorite_goods_id=goods_id where favorite_user_id=? and favorite_goods_id=?";
		List<HashMap<String,Object>> list = SqlHelper.select1(sql,favoriteUserId, favoriteGoodsId);
		List<FavoriteGoods> favoriteList =utils.setAttrUtil.setFavoriteGoods(list);

		SqlHelper.close();	
		return favoriteList.get(0);
	}
	
	
	/**
	 * 收藏夹增加商品
	 * @param favoriteUserId
	 * @param favoriteGoodsId
	 * @return
	 */
	public int insert(String favoriteUserId ,String favoriteGoodsId){
		String sql="insert into  favorite(favorite_id,favorite_user_id,favorite_goods_id) values(uuid(),?,?)";
		int i=SqlHelper.update(sql,favoriteUserId, favoriteGoodsId);
		SqlHelper.close();
		return i;
	}
	
	/**
	 * 删除商品评论
	 * @param favoriteId
	 * @return
	 */
	public int delete(String  favoriteId){
		String sql="update favorite set favorite_status=0 where favorite_id=? ";
		int i=SqlHelper.update(sql,favoriteId);
		SqlHelper.close();
		return i;
	}
	
	
}
