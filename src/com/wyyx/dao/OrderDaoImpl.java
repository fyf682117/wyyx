package com.wyyx.dao;

import java.util.HashMap;
import java.util.List;

import com.wyyx.model.Order;

import utils.SqlHelper;

/**
 * 订单DAO操作
 * 
 * @author zzd19
 *
 */
public class OrderDaoImpl {
	/**
	 * 查询所有订单
	 * 
	 * @return
	 */
	public List<Order> orderSelect(){
		String sql="select * from order";
		List<HashMap<String,Object>> list = SqlHelper.select1(sql);
		List<Order> orders = utils.setAttrUtil.setOrder(list);
		SqlHelper.close();
		return orders;
	}
	/**
	 * 根据订单ID查询单个订单
	 * 
	 * @param orderId
	 * @return
	 */
	public Order orderSelectId(String orderId){
		String sql="select * from order where order_id=?";
		List<HashMap<String,Object>> list = SqlHelper.select1(sql, orderId);
		List<Order> orders = utils.setAttrUtil.setOrder(list);
		SqlHelper.close();
		return orders.get(0);
	}
	/**
	 * 查询用户下的所有订单
	 * 
	 * @param orderUserId
	 * @return
	 */
	public List<Order> orderSelectUser(String orderUserId){
		String sql="select * from order where order_user_id=?";
		List<HashMap<String,Object>> list = SqlHelper.select1(sql, orderUserId);
		List<Order> orders = utils.setAttrUtil.setOrder(list);
		SqlHelper.close();
		return orders;
	}
	/**
	 * 查询用户下的各种状态的订单
	 * 
	 * @param orderUserId
	 * @return
	 */
	public List<Order> orderSelectStatus(String orderUserId, String orderStatus){
		String sql="select * from order where order_user_id=? and order_status=?";
		List<HashMap<String,Object>> list = SqlHelper.select1(sql, orderUserId, orderStatus);
		List<Order> orders = utils.setAttrUtil.setOrder(list);
		SqlHelper.close();
		return orders;
	}
	/**
	 * 添加订单
	 * 
	 * @param orderUserId
	 * @param orderAddress
	 * @param orderUser
	 * @return
	 */
	public int orderAdd(String orderUserId, String orderAddress, String orderUser){
		String sql="INSERT INTO order(order_id,order_user_id,order_address,order_user)VALUES(uuid(),?,?,?)";
		int i = SqlHelper.update(sql, orderUserId, orderAddress, orderUser);
		SqlHelper.close();
		return i;
	}
	/**
	 * 更新订单状态
	 * 
	 * @param orderId
	 * @param orderStatus
	 * @return
	 */
	public int orderDelete(String orderId, String orderStatus) {
		String sql = "UPDATE order SET order_status=? WHERE order_id = ?";
		int i = SqlHelper.update(sql, orderStatus, orderId);
		SqlHelper.close();
		return i;
	}
	/**
	 * 更新收件地址收件人
	 * 
	 * @param orderId
	 * @param orderStatus
	 * @return
	 */
	public int orderUpdate(String orderId, String orderAddress, String orderUser) {
		String sql = "UPDATE order SET order_address=?,order_user=? WHERE order_id = ?";
		int i = SqlHelper.update(sql, orderAddress, orderUser, orderId);
		SqlHelper.close();
		return i;
	}
}
