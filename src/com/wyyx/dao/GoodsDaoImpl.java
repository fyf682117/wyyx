package com.wyyx.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.wyyx.model.Goods;

import utils.SqlHelper;
import utils.StringUtils;
import utils.setAttrUtil;

/**
 * 商品dao层
 * @author 小菜
 *
 */
public class GoodsDaoImpl {
	/**
	 * 需要传入商品属性
	 * 向商品表中添加商品
	 * @param goods 一个商品属性
	 */
	
	
	public int insertGoods(Goods goods) {
		String sql = "insert into goods values(uuid(),?,?,?,?,?,?,?,?,?,now(),?,?,?,?,?,?)";
		int i= SqlHelper.update(sql,goods.getGoodsName(),goods.getGoodsPrice(),goods.getGoodsUnit(),goods.getGoodsSize(),goods.getGoodsColor(),goods.getGoodsImage(),goods.getGoodsStock(),goods.getGoodsAddress(),goods.getGoodsDiscount(),goods.getGoodsClickNumber(),goods.getGoodsCollectionNumber(),goods.getGoodsBuyNumber(),goods.getGoodComment(),goods.getGoodsSmallClassId(),goods.getGoodsStatus());
		SqlHelper.close();
		return i;
	}
	/**
	 * 根据商品id删除商品
	 * @param goodsId
	 */
	
	public int deleteGoods(String goodsId) {
		String sql = "update goods set goods_status = 0 where goods_id = ?";
		int i = SqlHelper.update(sql,goodsId);
		SqlHelper.close();
		return i;
	}
	
	/**
	 * 修改商品属性
	 * @param goods
	 */
	public int updateGoods(Goods goods) {
		String sql = "update goods set goods_price = ? ,goods_stock = ? ,goods_comment= ?,goods_image=? where goods_id = ?";
		int i = SqlHelper.update(sql, goods.getGoodsPrice(),goods.getGoodsStock(),goods.getGoodComment(),goods.getGoodsImage(),goods.getGoodsId());
		SqlHelper.close();
		return i;
	}
	
	/**
	 * 单个商品详情
	 */
	public Goods selcetOneGoods(String goodsId) {
		String sql ="select * from goods where goods_id = ?";
		List<HashMap<String,Object>> list = SqlHelper.select1(sql, goodsId);
		List<Goods> goods = setAttrUtil.setGoods(list);
		return goods.get(0);
	}
	/**
	 * 根据搜索框模糊搜索
	 * @param goodsName
	 * @return
	 */
	public List<Goods> selectByName(String goodsName){
		String sql = "select * from goods where goods_name like '%"+goodsName+"%' limit 5";
		List<HashMap<String,Object>> list = SqlHelper.select1(sql);
		//Goods goods = null;
		List<Goods> goodsList = setAttrUtil.setGoods(list);
		SqlHelper.close();
		return goodsList;
	}
	/**
	 * 根据大类别搜索
	 * @param large_class_id
	 * @return
	 */
	public List<Goods> selectByLargeClass(String large_class_id){
		String sql = "SELECT g.* from goods g LEFT JOIN small_class s on s.small_class_id = g.goods_small_class_id WHERE small_class_large_class_id = ? limit 4";
		List<HashMap<String,Object>> list = SqlHelper.select1(sql,large_class_id);
		List<Goods> largeGoodsList = setAttrUtil.setGoods(list);
		SqlHelper.close();
		return largeGoodsList;
		
	}
	
	/**
	 * 根据小类别id查询
	 * @param large_class_id
	 * @return
	 */
	public List<Goods> selectBySmallClass(String small_class_id){
		String sql = "select * from goods where goods_small_class_id = ? limit 5";
		List<HashMap<String,Object>> list = SqlHelper.select1(sql,small_class_id);
		List<Goods> smallGoodsList = setAttrUtil.setGoods(list);
		SqlHelper.close();
		return smallGoodsList;
		
	}
	
	/**
	 * 根据新品查询
	 * @param large_class_id
	 * @return
	 */
	public List<Goods> selectByNewGoods(){
		String sql = "SELECT * from goods order by goods_add_time desc limit 5";
		List<HashMap<String,Object>> list = SqlHelper.select1(sql);
		List<Goods> newGoodsList = setAttrUtil.setGoods(list);
		SqlHelper.close();
		return newGoodsList;
		
	}
	
	/**
	 * 根据热度查询
	 * @return
	 */
	public List<Goods> selectByHot(){
		String sql = "select * from goods order by goods_click_number limit 5";
		List<HashMap<String,Object>> list = SqlHelper.select1(sql);
		List<Goods> newGoodsList = setAttrUtil.setGoods(list);
		SqlHelper.close();
		return newGoodsList;
	}
	
}
