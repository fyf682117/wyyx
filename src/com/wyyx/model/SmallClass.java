package com.wyyx.model;

public class SmallClass {
	private String smallClassId;
	private String smallClassLargeClassId;
	private String smallClassName;
	private String smallClassTime;
	private String smallClassStatus;
	public String getSmallClassId() {
		return smallClassId;
	}
	public String getSmallClassLargeClassId() {
		return smallClassLargeClassId;
	}
	public String getSmallClassName() {
		return smallClassName;
	}
	public String getSmallClassStatus() {
		return smallClassStatus;
	}
	public String getSmallClassTime() {
		return smallClassTime;
	}
	public void setSmallClassId(String smallClassId) {
		this.smallClassId = smallClassId;
	}
	public void setSmallClassLargeClassId(String smallClassLargeClassId) {
		this.smallClassLargeClassId = smallClassLargeClassId;
	}
	public void setSmallClassName(String smallClassName) {
		this.smallClassName = smallClassName;
	}
	public void setSmallClassStatus(String smallClassStatus) {
		this.smallClassStatus = smallClassStatus;
	}
	public void setSmallClassTime(String smallClassTime) {
		this.smallClassTime = smallClassTime;
	}
	
	
}
