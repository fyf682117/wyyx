package com.wyyx.model;

public class Favorite {
	private String favoriteId;
	private String favoriteUserId;
	private String favoriteGoodsId;
	private String favoriteAddTime;
	private String favoriteStatus;
	public String getFavoriteAddTime() {
		return favoriteAddTime;
	}
	public String getFavoriteGoodsId() {
		return favoriteGoodsId;
	}
	public String getFavoriteId() {
		return favoriteId;
	}
	public String getFavoriteStatus() {
		return favoriteStatus;
	}
	public String getFavoriteUserId() {
		return favoriteUserId;
	}
	public void setFavoriteAddTime(String favoriteAddTime) {
		this.favoriteAddTime = favoriteAddTime;
	}
	public void setFavoriteGoodsId(String favoriteGoodsId) {
		this.favoriteGoodsId = favoriteGoodsId;
	}
	public void setFavoriteId(String favoriteId) {
		this.favoriteId = favoriteId;
	}
	public void setFavoriteStatus(String favoriteStatus) {
		this.favoriteStatus = favoriteStatus;
	}
	public void setFavoriteUserId(String favoriteUserId) {
		this.favoriteUserId = favoriteUserId;
	}
	
}
