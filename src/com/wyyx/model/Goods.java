package com.wyyx.model;

public class Goods {
	private String goodsId;
	private String goodsName;
	private String goodsPrice;
	private String goodsUnit;
	private String goodsSize;
	private String goodsColor;
	private String goodsImage;
	private String goodsStock;
	private String goodsAddress;
	private String goodsDiscount;
	private String goodsAddTime;
	private String goodsClickNumber;
	private String goodsCollectionNumber;
	private String goodsBuyNumber;
	private String goodsComment;
	private String goodsSmallClassId;
	private String goodsStatus;
	public String getGoodComment() {
		return goodsComment;
	}
	public String getGoodsAddress() {
		return goodsAddress;
	}
	public String getGoodsAddTime() {
		return goodsAddTime;
	}
	public String getGoodsBuyNumber() {
		return goodsBuyNumber;
	}
	public String getGoodsClickNumber() {
		return goodsClickNumber;
	}
	public String getGoodsCollectionNumber() {
		return goodsCollectionNumber;
	}
	public String getGoodsColor() {
		return goodsColor;
	}
	public String getGoodsDiscount() {
		return goodsDiscount;
	}
	public String getGoodsId() {
		return goodsId;
	}
	public String getGoodsImage() {
		return goodsImage;
	}
	public String getGoodsName() {
		return goodsName;
	}
	public String getGoodsPrice() {
		return goodsPrice;
	}
	public String getGoodsSize() {
		return goodsSize;
	}
	public String getGoodsSmallClassId() {
		return goodsSmallClassId;
	}
	public String getGoodsStatus() {
		return goodsStatus;
	}
	public String getGoodsStock() {
		return goodsStock;
	}
	public String getGoodsUnit() {
		return goodsUnit;
	}
	public void setGoodComment(String goodComment) {
		this.goodsComment = goodComment;
	}
	public void setGoodsAddress(String goodsAddress) {
		this.goodsAddress = goodsAddress;
	}
	public void setGoodsAddTime(String goodsAddTime) {
		this.goodsAddTime = goodsAddTime;
	}
	public void setGoodsBuyNumber(String goodsBuyNumber) {
		this.goodsBuyNumber = goodsBuyNumber;
	}
	public void setGoodsClickNumber(String goodsClickNumber) {
		this.goodsClickNumber = goodsClickNumber;
	}
	public void setGoodsCollectionNumber(String goodsCollectionNumber) {
		this.goodsCollectionNumber = goodsCollectionNumber;
	}
	public void setGoodsColor(String goodsColor) {
		this.goodsColor = goodsColor;
	}
	public void setGoodsDiscount(String goodsDiscount) {
		this.goodsDiscount = goodsDiscount;
	}
	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}
	public void setGoodsImage(String goodsImage) {
		this.goodsImage = goodsImage;
	}
	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	public void setGoodsPrice(String goodsPrice) {
		this.goodsPrice = goodsPrice;
	}
	public void setGoodsSize(String goodsSize) {
		this.goodsSize = goodsSize;
	}
	public void setGoodsSmallClassId(String goodsSmallClassId) {
		this.goodsSmallClassId = goodsSmallClassId;
	}
	public void setGoodsStatus(String goodsStatus) {
		this.goodsStatus = goodsStatus;
	}
	public void setGoodsStock(String goodsStock) {
		this.goodsStock = goodsStock;
	}
	public void setGoodsUnit(String goodsUnit) {
		this.goodsUnit = goodsUnit;
	}
}
