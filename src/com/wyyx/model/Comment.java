package com.wyyx.model;

public class Comment {
	private String commentId;
	private String commentGoodsId;
	private String commentUserId;
	private String commentContent;
	private String commentAddTime;
	private String commentStatus;
	public String getCommentAddTime() {
		return commentAddTime;
	}
	public String getCommentContent() {
		return commentContent;
	}
	public String getCommentGoodsId() {
		return commentGoodsId;
	}
	public String getCommentId() {
		return commentId;
	}
	public String getCommentStatus() {
		return commentStatus;
	}
	public String getCommentUserId() {
		return commentUserId;
	}
	public void setCommentAddTime(String commentAddTime) {
		this.commentAddTime = commentAddTime;
	}
	public void setCommentContent(String commentContent) {
		this.commentContent = commentContent;
	}
	public void setCommentGoodsId(String commentGoodsId) {
		this.commentGoodsId = commentGoodsId;
	}
	public void setCommentId(String commentId) {
		this.commentId = commentId;
	}
	public void setCommentStatus(String commentStatus) {
		this.commentStatus = commentStatus;
	}
	public void setCommentUserId(String commentUserId) {
		this.commentUserId = commentUserId;
	}
}
