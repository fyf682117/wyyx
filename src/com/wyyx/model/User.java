package com.wyyx.model;

public class User {
	
	private String userId;
	private String userName;
	private String userRealName;
	private String userPassword;
	private String userIdNumber;
	private String userTelphone;
	private String userEmail;
	private String userAddress;
	private String userSex;
	private String userBirthday;
	private String userScore;
	private String userImage;
	private String userRegisterTime;
	private String userStatus;
	public String getUserAddress() {
		return userAddress;
	}
	public String getUserBirthday() {
		return userBirthday;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public String getUserId() {
		return userId;
	}
	public String getUserIdNumber() {
		return userIdNumber;
	}
	public String getUserImage() {
		return userImage;
	}
	public String getUserName() {
		return userName;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public String getUserRealName() {
		return userRealName;
	}
	public String getUserRegisterTime() {
		return userRegisterTime;
	}
	public String getUserScore() {
		return userScore;
	}
	public String getUserSex() {
		return userSex;
	}
	public String getUserStatus() {
		return userStatus;
	}
	public String getUserTelphone() {
		return userTelphone;
	}
	public void setUserAddress(String userAddress) {
		this.userAddress = userAddress;
	}
	public void setUserBirthday(String userBirthday) {
		this.userBirthday = userBirthday;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public void setUserIdNumber(String userIdNumber) {
		this.userIdNumber = userIdNumber;
	}
	public void setUserImage(String userImage) {
		this.userImage = userImage;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	public void setUserRealName(String userRealName) {
		this.userRealName = userRealName;
	}
	public void setUserRegisterTime(String userRegisterTime) {
		this.userRegisterTime = userRegisterTime;
	}
	public void setUserScore(String userScore) {
		this.userScore = userScore;
	}
	public void setUserSex(String userSex) {
		this.userSex = userSex;
	}
	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}
	public void setUserTelphone(String userTelphone) {
		this.userTelphone = userTelphone;
	}
	
	
	

}
