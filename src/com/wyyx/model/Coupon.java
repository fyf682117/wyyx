package com.wyyx.model;

public class Coupon {
	private String couponId;
	private String couponUserId;
	private String couponOrderId;
	private String couponName;
	private String couponDiscountPrice;
	private String couponLimitPrice;
	private String couponStartTime;
	private String couponeEndTime;
	private String couponStatus;
	public String getCouponDiscountPrice() {
		return couponDiscountPrice;
	}
	public String getCouponeEndTime() {
		return couponeEndTime;
	}
	public String getCouponId() {
		return couponId;
	}
	public String getCouponLimitPrice() {
		return couponLimitPrice;
	}
	public String getCouponName() {
		return couponName;
	}
	public String getCouponOrderId() {
		return couponOrderId;
	}
	public String getCouponStartTime() {
		return couponStartTime;
	}
	public String getCouponStatus() {
		return couponStatus;
	}
	public String getCouponUserId() {
		return couponUserId;
	}
	public void setCouponDiscountPrice(String couponDiscountPrice) {
		this.couponDiscountPrice = couponDiscountPrice;
	}
	public void setCouponeEndTime(String couponeEndTime) {
		this.couponeEndTime = couponeEndTime;
	}
	public void setCouponId(String couponId) {
		this.couponId = couponId;
	}
	public void setCouponLimitPrice(String couponLimitPrice) {
		this.couponLimitPrice = couponLimitPrice;
	}
	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}
	public void setCouponOrderId(String couponOrderId) {
		this.couponOrderId = couponOrderId;
	}
	public void setCouponStartTime(String couponStartTime) {
		this.couponStartTime = couponStartTime;
	}
	public void setCouponStatus(String couponStatus) {
		this.couponStatus = couponStatus;
	}
	public void setCouponUserId(String couponUserId) {
		this.couponUserId = couponUserId;
	}
	
}
