package com.wyyx.model;

public class LargeClass {
	private String largeClassId;
	private String largeClassName;
	private String largeClassTime;
	private String largeClassStatus;
	public String getLargeClassId() {
		return largeClassId;
	}
	public String getLargeClassName() {
		return largeClassName;
	}
	public String getLargeClassStatus() {
		return largeClassStatus;
	}
	public String getLargeClassTime() {
		return largeClassTime;
	}
	public void setLargeClassId(String largeClassId) {
		this.largeClassId = largeClassId;
	}
	public void setLargeClassName(String largeClassName) {
		this.largeClassName = largeClassName;
	}
	public void setLargeClassStatus(String largeClassStatus) {
		this.largeClassStatus = largeClassStatus;
	}
	public void setLargeClassTime(String largeClassTime) {
		this.largeClassTime = largeClassTime;
	}
	
}
