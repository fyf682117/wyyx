package com.wyyx.model;

public class GoodsOrder {
	private String goId;
	private String goOrderId;
	private String goGoodsId;
	private String goGoodsCount;
	public String getGoGoodsCount() {
		return goGoodsCount;
	}
	public String getGoGoodsId() {
		return goGoodsId;
	}
	public String getGoId() {
		return goId;
	}
	public String getGoOrderId() {
		return goOrderId;
	}
	public void setGoGoodsCount(String goGoodsCount) {
		this.goGoodsCount = goGoodsCount;
	}
	public void setGoGoodsId(String goGoodsId) {
		this.goGoodsId = goGoodsId;
	}
	public void setGoId(String goId) {
		this.goId = goId;
	}
	public void setGoOrderId(String goOrderId) {
		this.goOrderId = goOrderId;
	}
	
}
