package com.wyyx.model;

public class Order {
	private String OrderId;
	private String OrderUserId;
	private String OrderAddress;
	private String OrderAddTime;
	private String OrderStatus;
	public String getOrderAddress() {
		return OrderAddress;
	}
	public String getOrderAddTime() {
		return OrderAddTime;
	}
	public String getOrderId() {
		return OrderId;
	}
	public String getOrderStatus() {
		return OrderStatus;
	}
	public String getOrderUserId() {
		return OrderUserId;
	}
	public void setOrderAddress(String orderAddress) {
		OrderAddress = orderAddress;
	}
	public void setOrderAddTime(String orderAddTime) {
		OrderAddTime = orderAddTime;
	}
	public void setOrderId(String orderId) {
		OrderId = orderId;
	}
	public void setOrderStatus(String orderStatus) {
		OrderStatus = orderStatus;
	}
	public void setOrderUserId(String orderUserId) {
		OrderUserId = orderUserId;
	}
	
}
