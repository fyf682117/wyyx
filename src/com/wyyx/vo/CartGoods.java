package com.wyyx.vo;

import com.wyyx.model.Goods;

public class CartGoods {
	private String cartId;
	private String cartUserId;
	private String cartGoodsId;
	private String cartGoodsNumber;
	private String cartAddTime;
	private String cartStatus;
	Goods goods = new Goods();
	public Goods getGoods() {
		return goods;
	}
	public void setGoods(Goods goods) {
		this.goods=goods;
	}
	public String getCartAddTime() {
		return cartAddTime;
	}
	public String getCartGoodsId() {
		return cartGoodsId;
	}
	public String getCartGoodsNumber() {
		return cartGoodsNumber;
	}
	public String getCartId() {
		return cartId;
	}
	public String getCartStatus() {
		return cartStatus;
	}
	public String getCartUserId() {
		return cartUserId;
	}
	public void setCartAddTime(String cartAddTime) {
		this.cartAddTime = cartAddTime;
	}
	public void setCartGoodsId(String cartGoodsId) {
		this.cartGoodsId = cartGoodsId;
	}
	public void setCartGoodsNumber(String cartGoodsNumber) {
		this.cartGoodsNumber = cartGoodsNumber;
	}
	public void setCartId(String cartId) {
		this.cartId = cartId;
	}
	public void setCartStatus(String cartStatus) {
		this.cartStatus = cartStatus;
	}
	public void setCartUserId(String cartUserId) {
		this.cartUserId = cartUserId;
	}
}
