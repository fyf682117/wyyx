package com.wyyx.vo;

import com.wyyx.model.Goods;

public class FavoriteGoods {
	private String favoriteId;
	private String favoriteUserId;
	private String favoriteGoodsId;
	private String favoriteAddTime;
	private String favoriteStatus;
	Goods goods =new Goods();
	public String getFavoriteAddTime() {
		return favoriteAddTime;
	}
	public String getFavoriteGoodsId() {
		return favoriteGoodsId;
	}
	public String getFavoriteId() {
		return favoriteId;
	}
	public String getFavoriteStatus() {
		return favoriteStatus;
	}
	public String getFavoriteUserId() {
		return favoriteUserId;
	}
	public Goods getGoods() {
		return goods;
	}
	public void setFavoriteAddTime(String favoriteAddTime) {
		this.favoriteAddTime = favoriteAddTime;
	}
	public void setFavoriteGoodsId(String favoriteGoodsId) {
		this.favoriteGoodsId = favoriteGoodsId;
	}
	public void setFavoriteId(String favoriteId) {
		this.favoriteId = favoriteId;
	}
	public void setFavoriteStatus(String favoriteStatus) {
		this.favoriteStatus = favoriteStatus;
	}
	public void setFavoriteUserId(String favoriteUserId) {
		this.favoriteUserId = favoriteUserId;
	}
	public void setGoods(Goods goods) {
		this.goods = goods;
	}
}
