package com.wyyx.services;

import java.util.List;

import com.wyyx.dao.CommentDaoImpl;
import com.wyyx.model.Comment;

public class CommentServices {
	/**
	 *  查看用户所有评论
	 * @param commentUserId
	 * @return
	 */
	public List<Comment> selectAll(String commentUserId){
		CommentDaoImpl commentDaoImpl =new CommentDaoImpl();
		
		return commentDaoImpl.selectAll(commentUserId);
	}
	/**
	 * 查看商品的所有评论
	 * @param commentGoodsId
	 * @return
	 */
	public List<Comment> selectAll2(String commentGoodsId){
		CommentDaoImpl commentDaoImpl =new CommentDaoImpl();
		return commentDaoImpl.selectAll2(commentGoodsId);
	}
	/**
	 * 添加商品评论
	 * @param commentGoodsId
	 * @param commentUserId
	 * @param commentContent
	 * @return
	 */
	public  int insert(String commentGoodsId,String commentUserId,String commentContent){
		CommentDaoImpl commentDaoImpl =new CommentDaoImpl();
		return commentDaoImpl.insert(commentGoodsId, commentUserId, commentContent);
	}
	/**
	 *  删除评论
	 * @param commentStatus
	 * @param commentId
	 * @return
	 */
	public int delect(String commentId){
		CommentDaoImpl commentDaoImpl =new CommentDaoImpl();
		return commentDaoImpl.delect("0", commentId);
	}
}
