package com.wyyx.services;

import com.wyyx.dao.UserDaoImpl;
import com.wyyx.model.User;
/**
 * 用户登录业务层
 * 
 * @author zzd19
 *
 */
public class UserLoginServices {
	/**
	 * 使用手机号码登录
	 * 
	 * @param userTelephone
	 * @return
	 */
	public User userLogin(String userTelephone){
		UserDaoImpl userDaoImpl=new  UserDaoImpl();
		return userDaoImpl.userLoginSelect(userTelephone);
	}
	/**
	 * 使用邮箱登录
	 * 
	 * @param userEmail
	 * @param userPassword
	 * @return
	 */
	public User userLogin(String userEmail, String userPassword) {
		UserDaoImpl userDaoImpl=new  UserDaoImpl();
		return userDaoImpl.userLoginSelect(userEmail,userPassword);
	}
}
