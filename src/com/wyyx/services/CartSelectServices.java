package com.wyyx.services;

import java.util.List;

import com.wyyx.dao.CartDaoImpl;
import com.wyyx.vo.CartGoods;

/**
 * 查看购物车业务层
 * 
 * @author zzd19
 *
 */
public class CartSelectServices {
	/**
	 * 查询所有购物车
	 * 
	 * @return
	 */
	public List<CartGoods> cartSelectAll(){
		CartDaoImpl cartDaoImpl = new CartDaoImpl();
		return cartDaoImpl.cartSelectAll();
	}
	/**
	 * 查询单个购物车记录
	 * 
	 * @param cartId
	 * @return
	 */
	public List<CartGoods> cartSelectOne(String cartId){
		CartDaoImpl cartDaoImpl = new CartDaoImpl();
		return cartDaoImpl.cartSelectUser(cartId);
	}
    /**
     * 查询用户下的购物车
     * 
     * @param cartUserId
     * @return
     */
	public List<CartGoods> cartSelectUser(String cartUserId){
		CartDaoImpl cartDaoImpl = new CartDaoImpl();
		return cartDaoImpl.cartSelectUser(cartUserId);
	}
}
