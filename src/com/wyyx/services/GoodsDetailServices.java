package com.wyyx.services;

import com.wyyx.dao.GoodsDaoImpl;
import com.wyyx.model.Goods;

/**
 * 单个商品详情
 * @author 小菜
 *
 */
public class GoodsDetailServices {
	public Goods goodsDetail(String goodsId) {
		GoodsDaoImpl goodsDaoImpl = new GoodsDaoImpl();
		return goodsDaoImpl.selcetOneGoods(goodsId);
	}
}
