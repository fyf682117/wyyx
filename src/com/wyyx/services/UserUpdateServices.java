package com.wyyx.services;

import com.wyyx.dao.UserDaoImpl;
/**
 * 更新用户个人信息
 * 
 * @author zzd19
 *
 */
public class UserUpdateServices {
	/**
	 * 更新个人信息
	 * 
	 * @param userId
	 * @param userUserName
	 * @param userTelphone
	 * @param userEmail
	 * @param userAddress
	 * @param userSex
	 * @param userBirthday
	 * @return
	 */
	public int update(String userId, String userUserName, String userTelphone, String userEmail, String userAddress, String userSex, String userBirthday) {
		UserDaoImpl userDaoImpl = new UserDaoImpl();
		return userDaoImpl.update(userId, userUserName, userTelphone, userEmail, userAddress, userSex, userBirthday);
	}
	/**
	 * 实名认证
	 * 
	 * @param userId
	 * @param userRealName
	 * @param userIdNumber
	 * @return 
	 */
	public int updateAuthentication(String userId, String userRealName, String userIdNumber) {
		UserDaoImpl userDaoImpl = new UserDaoImpl();
		//获取身份证号码
		String id = userDaoImpl.userSelectId(userId).getUserIdNumber()==null?"":userDaoImpl.userSelectId(userId).getUserIdNumber();
		//对身份证号码进行判断
		if(id.equals("")) {
			return userDaoImpl.updateAuthentication(userId, userRealName, userIdNumber);
		}
		return 0;
	}
	/**
	 * 更改密码
	 * 
	 * @param userId
	 * @param userPassword
	 * @return
	 */
	public int updatePassword(String userId, String userPassword ) {
		UserDaoImpl userDaoImpl = new UserDaoImpl();
		//获取旧密码
		String password = userDaoImpl.userSelectId(userId).getUserPassword()==null?"":userDaoImpl.userSelectId(userId).getUserPassword();
		//对新旧密码进行比较
		if(password.equals(userPassword)) {
			return userDaoImpl.updatePassword(userId, userPassword);
		}
		return 0;
	}
	/**
	 * 更新积分
	 * 
	 * @param userId
	 * @param userScore
	 * @return
	 */
	public int updateScore(String userId, String userScore ) {
		UserDaoImpl userDaoImpl = new UserDaoImpl();
		return userDaoImpl.updateScore(userId, userScore);
	}
	/**
	 * 更新状态
	 * 
	 * @param userId
	 * @param userScore
	 * @return
	 */
	public int updateStatus(String userId, String userStatus ) {
		UserDaoImpl userDaoImpl = new UserDaoImpl();
		return userDaoImpl.updateStatus(userId, userStatus);
	}
}
