package com.wyyx.services;

import com.wyyx.dao.GoodsDaoImpl;

/**
 * 删除商品服务
 * @author 小菜
 *
 */
public class DeleteGoodsServices {
	//删除单个商品
	public int deleteGoods(String goodsId) {
		GoodsDaoImpl goodsDaoImpl = new GoodsDaoImpl();
		return goodsDaoImpl.deleteGoods(goodsId);
		  
	}
}
