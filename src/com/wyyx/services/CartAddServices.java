package com.wyyx.services;

import java.util.List;

import com.wyyx.dao.CartDaoImpl;
import com.wyyx.dao.GoodsDaoImpl;
import com.wyyx.model.Goods;
import com.wyyx.vo.CartGoods;

/**
 * 添加购物车业务层
 * 
 * @author zzd19
 *
 */
public class CartAddServices {
    /**
     * 
     * @param cartUserId
     * @param cartGoodsId
     * @param cartGoodsNumber
     * @return
     */
	public int cartAdd(String cartUserId, String cartGoodsId, String cartGoodsNumber) {
		CartDaoImpl cartDaoImpl = new CartDaoImpl();
		//查询购物车中是否有此商品
		List<CartGoods> carts = cartDaoImpl.cartSelectGoods(cartUserId, cartGoodsId);
		if(carts.isEmpty()) {
			//如果不存在就添加商品
			CartGoods cartGoods = carts.get(0);    //得到购物车记录
			String goodsId = cartGoods.getCartGoodsId();    //得到购物车中商品ID
			GoodsDaoImpl goodsDaoImpl = new GoodsDaoImpl();
			Goods good = goodsDaoImpl.selcetOneGoods(goodsId);    //得到此件商品
			String goodPrice = good.getGoodsPrice();    //获取此件商品单价
			double price = Double.parseDouble(goodPrice);    //将商品单价转换为double类型
			double allPrice = price*Integer.parseInt(cartGoodsNumber);    //获取单条订单商品总价
			String i = utils.StringUtils.valueOf(allPrice);    //将总价转换为字符串类型
			return cartDaoImpl.cartAdd(cartUserId, cartGoodsId, cartGoodsNumber, i);
		}else {
			//如果存在就更改数量
			CartGoods cartGoods = carts.get(0);   //得到购物车记录
			String cartId = cartGoods.getCartId();    //得到购物车ID
			String number = cartGoods.getCartGoodsNumber();    //得到商品数量
			int num = Integer.parseInt(number)+Integer.parseInt(cartGoodsNumber);    //商品相加得数量
			String numTrue = String.valueOf(num);    //转换为字符串类型
			String goodsId = cartGoods.getCartGoodsId();    //得到购物车中商品ID
			GoodsDaoImpl goodsDaoImpl = new GoodsDaoImpl();
			Goods good = goodsDaoImpl.selcetOneGoods(goodsId);    //得到此件商品
			String goodPrice = good.getGoodsPrice();    //获取此件商品单价
			double price = Double.parseDouble(goodPrice);    //将商品单价转换为double类型
			double allPrice = price*num;    //获取单条订单总价
			String i = utils.StringUtils.valueOf(allPrice);    //将总价转换为字符串类型
			return cartDaoImpl.cartNumber(cartId, numTrue, i);
		}
	}
}
