package com.wyyx.services;

import com.wyyx.dao.CartDaoImpl;
import com.wyyx.dao.GoodsDaoImpl;
import com.wyyx.model.Goods;
import com.wyyx.vo.CartGoods;

/**
 * 更新购物车业务操作
 * 
 * @author zzd19
 *
 */
public class CartUpdateServices {
	/**
	 * 商品数量加一
	 * 
	 * @param cartId
	 * @return
	 */
    public int cartAddOne(String cartId) {
    	CartDaoImpl cartDaoImpl = new CartDaoImpl();
    	CartGoods cartGoods = cartDaoImpl.cartSelectId(cartId);    //得到此购物车
		String number = cartGoods.getCartGoodsNumber();    //得到商品数量
		int num = Integer.parseInt(number)+1;    //商品相加得数量
		String numTrue = String.valueOf(num);    //转换为字符串类型
		
		String goodsId = cartGoods.getCartGoodsId();    //得到购物车中商品ID
		GoodsDaoImpl goodsDaoImpl = new GoodsDaoImpl();
		Goods good = goodsDaoImpl.selcetOneGoods(goodsId);    //得到此件商品
		String goodPrice = good.getGoodsPrice();    //获取此件商品单价
		double price = Double.parseDouble(goodPrice);    //将商品单价转换为double类型
		double allPrice = price*num;    //获取单条订单总价
		String i = utils.StringUtils.valueOf(allPrice);    //将总价转换为字符串类型
		
		return cartDaoImpl.cartNumber(cartId, numTrue, i);
    }
    /**
	 * 商品数量减一
	 * 
	 * @param cartId
	 * @return
	 */
    public int cartJianOne(String cartId) {
    	CartDaoImpl cartDaoImpl = new CartDaoImpl();
    	CartGoods cartGoods = cartDaoImpl.cartSelectId(cartId);    //得到此购物车
		String number = cartGoods.getCartGoodsNumber();    //得到商品数量
		if(number=="1") {
			return 0;
		}
		int num = Integer.parseInt(number)-1;    //商品相加得数量
		String numTrue = String.valueOf(num);    //转换为字符串类型
		
		String goodsId = cartGoods.getCartGoodsId();    //得到购物车中商品ID
		GoodsDaoImpl goodsDaoImpl = new GoodsDaoImpl();
		Goods good = goodsDaoImpl.selcetOneGoods(goodsId);    //得到此件商品
		String goodPrice = good.getGoodsPrice();    //获取此件商品单价
		double price = Double.parseDouble(goodPrice);    //将商品单价转换为double类型
		double allPrice = price*num;    //获取单条订单总价
		String i = utils.StringUtils.valueOf(allPrice);    //将总价转换为字符串类型
		
		return cartDaoImpl.cartNumber(cartId, numTrue, i);
    }
}
