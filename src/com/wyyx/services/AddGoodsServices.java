package com.wyyx.services;

import com.wyyx.dao.GoodsDaoImpl;
import com.wyyx.model.Goods;

/**
 * 添加商品服务
 * @author 小菜
 *
 */
public class AddGoodsServices {
	public int  addGoods(Goods goods) {
		GoodsDaoImpl goodsDaoImpl = new GoodsDaoImpl();
		return goodsDaoImpl.insertGoods(goods);
	}
}
