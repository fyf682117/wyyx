package com.wyyx.services;

import java.util.List;

import com.wyyx.dao.FavoriteDaoImpl;
import com.wyyx.vo.FavoriteGoods;

public class FavoriteServices {
	/**
	 * 查看所有收藏
	 * @return
	 */
	public List<FavoriteGoods> selectAll(String favoriteUserId){
		FavoriteDaoImpl favoriteDaoImpl=new FavoriteDaoImpl();
		
		return favoriteDaoImpl.selectAll(favoriteUserId);
	}
	/**
	 * 查看收藏夹内单个商品信息
	 * @param favoriteGoodsId
	 * @return
	 */
	public FavoriteGoods selectone(String favoriteUserId,String favoriteGoodsId){
		FavoriteDaoImpl favoriteDaoImpl=new FavoriteDaoImpl();
		return favoriteDaoImpl.selectone(favoriteUserId, favoriteGoodsId);
		
	}
	
	/**
	 * 增加商品到收藏夹
	 * @param favoriteUserId
	 * @param favoriteGoodsId
	 * @return
	 */
	public int insert(String favoriteUserId,String favoriteGoodsId){
		FavoriteDaoImpl favoriteDaoImpl=new FavoriteDaoImpl();
		
		return favoriteDaoImpl.insert(favoriteUserId, favoriteGoodsId);
	}
	/**
	 * 删除
	 * @param favoriteId
	 * @return
	 */
	public int delete(String  favoriteId){
		FavoriteDaoImpl favoriteDaoImpl=new FavoriteDaoImpl();
		return favoriteDaoImpl.delete(favoriteId);
	}

}
