package com.wyyx.services;

import java.util.List;

import com.wyyx.dao.GoodsDaoImpl;
import com.wyyx.model.Goods;

/**
 * 根据搜索框代码查询
 * @author 小菜
 *
 */
public class GoodsSelectServices {
	GoodsDaoImpl goodsDaoImpl = new GoodsDaoImpl();
	
	/**
	 * 通过名字查询商品
	 * @param goodsName
	 * @return
	 */
	public List<Goods> selectGoodsByName(String goodsName) {
		return goodsDaoImpl.selectByName(goodsName);
	}
	/**
	 * 通过大类别查询
	 * @param largeId
	 * @return
	 */
	public List<Goods> selectGoodsBylargeId(String largeClassId) {
		return goodsDaoImpl.selectByLargeClass(largeClassId);
	}
	/**
	 * 通过小类别查询
	 * @param smallId
	 * @return
	 */
	public List<Goods> selectGoodsBySmallId(String smallClassId){
		return goodsDaoImpl.selectBySmallClass(smallClassId);
	}
	/**
	 * 通过新品查询
	 * @return
	 */
	public List<Goods> selectGoodsByNew(){
		return goodsDaoImpl.selectByNewGoods();
	}
	/**
	 * 通过热度查询
	 * @return
	 */
	public List<Goods> selectGoodsByHot(){
		return goodsDaoImpl.selectByHot();
	}
	
	
}
