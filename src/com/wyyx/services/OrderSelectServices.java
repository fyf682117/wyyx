package com.wyyx.services;

import java.util.List;

import com.wyyx.dao.OrderDaoImpl;
import com.wyyx.model.Order;

/**
 * 查询订单信息
 * 
 * @author zzd19
 *
 */
public class OrderSelectServices {
    /**
     * 查询所有订单信息
     * 
     * @return
     */
	public List<Order> orderSelect(){
		OrderDaoImpl orderDaoImpl = new OrderDaoImpl();
		return orderDaoImpl.orderSelect();
	}
	/**
	 * 根据订单ID查询单个订单
	 * 
	 * @param orderId
	 * @return
	 */
	public Order orderSelectId(String orderId) {
		OrderDaoImpl orderDaoImpl = new OrderDaoImpl();
		return orderDaoImpl.orderSelectId(orderId);
	}
	/**
	 * 查询用户下的所有订单
	 * 
	 * @param orderUserId
	 * @return
	 */
	public List<Order> orderSelectUser(String orderUserId) {
		OrderDaoImpl orderDaoImpl = new OrderDaoImpl();
		return orderDaoImpl.orderSelectUser(orderUserId);
	}
	/**
	 * 查询用户下的各种状态的订单
	 * 
	 * @param orderUserId
	 * @return
	 */
	//已取消
	public List<Order> orderSelectStatus0(String orderUserId, String orderStatus) {
		OrderDaoImpl orderDaoImpl = new OrderDaoImpl();
		return orderDaoImpl.orderSelectStatus(orderUserId, "0");
	}
	//已支付
	public List<Order> orderSelectStatus1(String orderUserId, String orderStatus) {
		OrderDaoImpl orderDaoImpl = new OrderDaoImpl();
		return orderDaoImpl.orderSelectStatus(orderUserId, "1");
	}
	//未支付
	public List<Order> orderSelectStatus2(String orderUserId, String orderStatus) {
		OrderDaoImpl orderDaoImpl = new OrderDaoImpl();
		return orderDaoImpl.orderSelectStatus(orderUserId, "2");
	}
}
