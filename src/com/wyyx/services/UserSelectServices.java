package com.wyyx.services;

import java.util.List;

import com.wyyx.dao.UserDaoImpl;
import com.wyyx.model.User;
/**
 * 用户查询业务层
 * 
 * @author zzd19
 *
 */
public class UserSelectServices {
	/**
	 * 进行查询所有用户操作
	 * 
	 * @param userTelephone
	 * @return
	 */
	public List<User> userSelect(){
		UserDaoImpl userDaoImpl=new  UserDaoImpl();
		return userDaoImpl.userSelect();
	}
	/**
	 * 使用用户ID进行查询操作
	 * 
	 * @param userTelephone
	 * @return
	 */
	public User userSelectId(String userId){
		UserDaoImpl userDaoImpl=new  UserDaoImpl();
		return userDaoImpl.userSelectId(userId);
	}
	/**
	 * 查询未删除状态用户操作
	 * 
	 * @param userTelephone
	 * @return
	 */
	public List<User> userSelectStatus1(){
		UserDaoImpl userDaoImpl=new  UserDaoImpl();
		return userDaoImpl.userSelectStatus("1");
	}
	/**
	 * 查询已删除状态用户操作
	 * 
	 * @param userTelephone
	 * @return
	 */
	public List<User> userSelectStatus0(){
		UserDaoImpl userDaoImpl=new  UserDaoImpl();
		return userDaoImpl.userSelectStatus("0");
	}
}
