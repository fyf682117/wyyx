package com.wyyx.services;

import com.wyyx.dao.UserDaoImpl;
/**
 * 用户注册业务层
 * 
 * @author zzd19
 *
 */
public class UserRegisterServices {
    /**
     * 用户注册方法
     * 
     * @param userUserName
     * @param userTelphone
     * @param userEmail
     * @param userPassword
     */
	public int register(String userUserName,String userTelphone,String userEmail,String userPassword) {
		UserDaoImpl userDaoImpl = new UserDaoImpl();
		return userDaoImpl.registerINSERT(userUserName, userTelphone, userEmail, userPassword);
	}
}
