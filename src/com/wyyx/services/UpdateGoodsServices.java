package com.wyyx.services;

import com.wyyx.dao.GoodsDaoImpl;
import com.wyyx.model.Goods;

/**
 * 更新商品服务
 * @author 小菜
 *
 */
public class UpdateGoodsServices {
	GoodsDaoImpl goodsDaoImpl = new GoodsDaoImpl();
	public int  updateGoods(Goods goods) {
		return goodsDaoImpl.updateGoods(goods);
	}
}
