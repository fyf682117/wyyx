package com.wyyx.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wyyx.model.Order;
import com.wyyx.services.OrderSelectServices;

import commons.Constants;
import commons.JsonResult;
import commons.JsonResultWriter;

/**
 * 查询所有订单信息
 */
@WebServlet("/api/OrderSelect")
public class OrderSelectAllServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JsonResult result =null;
		List<Order> orders = new ArrayList<Order>();
		try {
			OrderSelectServices orderSelectServices = new OrderSelectServices();
			orders = orderSelectServices.orderSelect();
			if (!orders.isEmpty()) {
				result=new JsonResult(orders, "查询成功",Constants.STATUS_SUCCESS);
			} else {
				result=new JsonResult("", "查询失败",Constants.STATUS_NOTFOUND);
			}
		} catch (Exception e) {
			result=new JsonResult(e.getMessage(), "查询异常", Constants.STATUS_EX);
		}
		JsonResultWriter.writer(response, result);
	}
}
