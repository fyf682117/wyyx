package com.wyyx.api;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wyyx.services.UserUpdateServices;

import commons.Constants;
import commons.JsonResult;
import commons.JsonResultWriter;

/**
 * 用户更新个人信息
 */
@WebServlet("/api/UserUpdate")
public class UserUpdateServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userId=request.getParameter("userId") ;
		String userUserName=request.getParameter("userUserName") ;
		String userTelphone=request.getParameter("userTelphone") ;
		String userEmail=request.getParameter("userEmail") ;
		String userAddress=request.getParameter("userAddress") ;
		String userSex=request.getParameter("userSex") ;
		String userBirthday=request.getParameter("userBirthday") ;
		JsonResult result =null;
		try {
			UserUpdateServices userUpdateServices = new UserUpdateServices();
			int num = userUpdateServices.update(userId, userUserName, userTelphone, userEmail, userAddress, userSex, userBirthday);
			if (num!=0) {
				result=new JsonResult("", "更新成功",Constants.STATUS_SUCCESS);
			} else {
				result=new JsonResult("", "请检查信息",Constants.STATUS_NOTFOUND);
			}
		} catch (Exception e) {
			result=new JsonResult(e.getMessage(), "更新失败", Constants.STATUS_EX);
		}
		JsonResultWriter.writer(response, result);
	}
}
