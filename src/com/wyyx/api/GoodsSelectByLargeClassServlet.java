package com.wyyx.api;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wyyx.model.Goods;
import com.wyyx.services.GoodsSelectServices;

import commons.Constants;
import commons.JsonResult;
import commons.JsonResultWriter;

/**
 * 根据大类别查询
 */
@WebServlet("/GoodsSelectByLargeClassServlet")
public class GoodsSelectByLargeClassServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String largeClassId = request.getParameter("largeClassId");
		GoodsSelectServices goodsSelectServices =new  GoodsSelectServices();
		List<Goods> list = goodsSelectServices.selectGoodsBylargeId(largeClassId);
		JsonResult result = null;
		try {
			if(list!=null) {
				result = new JsonResult<>(list,"查询成功",Constants.STATUS_SUCCESS);
				//System.out.println();
			}else {
				result = new JsonResult<>("查询失败",Constants.STATUS_NOTFOUND);
			}
		}catch(Exception e) {
			result = new JsonResult<>(e.getMessage(),"查询异常",Constants.STATUS_EX);
		}
		JsonResultWriter.writer(response,result);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
