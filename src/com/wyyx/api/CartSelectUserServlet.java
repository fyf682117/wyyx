package com.wyyx.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wyyx.model.Cart;
import com.wyyx.services.CartSelectServices;
import com.wyyx.vo.CartGoods;

import commons.Constants;
import commons.JsonResult;
import commons.JsonResultWriter;

/**
 * 查询用户下的购物车
 */
@WebServlet("/api/CartSelectUser")
public class CartSelectUserServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String cartUserId = request.getParameter("cartUserId");
		JsonResult result =null;
		List<CartGoods> carts = new ArrayList<CartGoods>();
		try {
			CartSelectServices cartSelectServices =new CartSelectServices();
			carts=cartSelectServices.cartSelectUser(cartUserId);
			if (!carts.isEmpty()) {
				result=new JsonResult(carts, "查询成功",Constants.STATUS_SUCCESS);
			} else {
				result=new JsonResult("", "查询失败",Constants.STATUS_NOTFOUND);
			}
		} catch (Exception e) {
			result=new JsonResult(e.getMessage(), "查询异常", Constants.STATUS_EX);
		}
		JsonResultWriter.writer(response, result);
	}
}
