package com.wyyx.api;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wyyx.services.UserUpdateServices;

import commons.Constants;
import commons.JsonResult;
import commons.JsonResultWriter;

/**
 * 重新添加用户
 */
@WebServlet("/api/UserUpdateStatus")
public class UserUpdateStatusServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userId=request.getParameter("userId") ;
		JsonResult result =null;
		try {
			UserUpdateServices userUpdateServices = new UserUpdateServices();
			int num = userUpdateServices.updateStatus(userId, "1");
			if (num!=0) {
				result=new JsonResult("", "删除成功",Constants.STATUS_SUCCESS);
			} else {
				result=new JsonResult("", "删除失败",Constants.STATUS_NOTFOUND);
			}
		} catch (Exception e) {
			result=new JsonResult(e.getMessage(), "操作出错", Constants.STATUS_EX);
		}
		JsonResultWriter.writer(response, result);
	}
}
