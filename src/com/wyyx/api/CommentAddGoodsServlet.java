package com.wyyx.api;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JWindow;

import com.wyyx.services.CommentServices;

import commons.Constants;
import commons.JsonResult;
import commons.JsonResultWriter;

/**
 * Servlet implementation class CommentAddGoodsServlet
 */
@WebServlet("/api/CommentAddGoodsServlet")
public class CommentAddGoodsServlet extends HttpServlet {
	/**
	 * 商品添加评论
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String commentGoodsId =request.getParameter("comment_goods_id");
		String commentUserId =request.getParameter("comment_user_id");
		String commentContent =request.getParameter("comment_content");
		JsonResult result= null;
		try {
			CommentServices commentServices =new CommentServices();
			int i =commentServices.insert(commentGoodsId, commentUserId, commentContent);
			if(i>0){
				result =new JsonResult(i, "添加评论成功", Constants.STATUS_SUCCESS);
			}else{
				result =new JsonResult("", "添加评论失败", Constants.STATUS_NOTFOUND);
			}
		} catch (Exception e) {
			result =new JsonResult(e.getMessage(), "添加评论异常", Constants.STATUS_EX);
		}
		JsonResultWriter.writer(response, result);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
