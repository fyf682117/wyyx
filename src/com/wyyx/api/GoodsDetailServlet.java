package com.wyyx.api;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wyyx.model.Goods;
import com.wyyx.services.GoodsDetailServices;

import commons.Constants;
import commons.JsonResult;
import commons.JsonResultWriter;

/**
 * Servlet implementation class GoodsDetailServlet
 */
@WebServlet("/GoodsDetailServlet")
public class GoodsDetailServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String goodsId = request.getParameter("goodsId");
		GoodsDetailServices goodsDetailServices = new GoodsDetailServices();
		Goods goods =goodsDetailServices.goodsDetail(goodsId);
		JsonResult result = null;
		
		try {
			if(goods != null) {
				result = new JsonResult<>(goods,"查询成功",Constants.STATUS_SUCCESS);
			}else {
				result = new JsonResult<>("查询失败",Constants.STATUS_NOTFOUND);
			}
		}catch(Exception e) {
			result = new JsonResult<>(e.getMessage(),"查询异常", Constants.STATUS_EX);
		}
		JsonResultWriter.writer(response,result);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
