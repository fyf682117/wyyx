package com.wyyx.api;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.wyyx.model.User;
import com.wyyx.services.UserLoginServices;
import com.wyyx.services.UserSelectServices;

import commons.Constants;
import commons.JsonResult;
import commons.JsonResultWriter;

/**
 * 根据用户ID查询用户
 */
@WebServlet("/api/UserSelectId")
public class UserSelectIdServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userId=request.getParameter("userId") ;
		JsonResult result =null;
		User user = null;
		try {
			UserSelectServices userSelectServices =new UserSelectServices();
			user=userSelectServices.userSelectId(userId);
			if (user!=null) {
				result=new JsonResult(user, "查询成功",Constants.STATUS_SUCCESS);
			} else {
				result=new JsonResult("", "该用户不存在",Constants.STATUS_NOTFOUND);
			}
		} catch (Exception e) {
			result=new JsonResult(e.getMessage(), "查询失败", Constants.STATUS_EX);
		}
		JsonResultWriter.writer(response, result);
	}
}
