package com.wyyx.api;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wyyx.services.CommentServices;

import commons.Constants;
import commons.JsonResult;
import commons.JsonResultWriter;

/**
 * Servlet implementation class CommentDeleteGoodsServlet
 */
@WebServlet("/api/CommentDeleteGoodsServlet")
public class CommentDeleteGoodsServlet extends HttpServlet {
	/**
	 * 删除商品评论表
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String commentId = request.getParameter("commentId");
		JsonResult result =null;
		
		try {
			CommentServices commentServices = new CommentServices();
			int i=commentServices.delect(commentId);
			if (i>0) {
				result =new JsonResult (i, "删除评论表成功", Constants.STATUS_SUCCESS);
			} else {
				result =new JsonResult("", "删除评论表失败", Constants.STATUS_NOTFOUND);
			}
		} catch (Exception e) {
			result =new JsonResult(e.getMessage(),"删除评论表异常", Constants.STATUS_EX);
		}
		JsonResultWriter.writer(response, result);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
