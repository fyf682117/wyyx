package com.wyyx.api;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.wyyx.services.FavoriteServices;
import commons.Constants;
import commons.JsonResult;
import commons.JsonResultWriter;

/**
 * Servlet implementation class FavoriteDeleteServlet
 */
@WebServlet("/api/FavoriteDeleteServlet")
public class FavoriteDeleteServlet extends HttpServlet {
	/**
	 * 收藏夹删除商品
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String favoriteId = request.getParameter("favoriteId");
		JsonResult result =null;
		try {
			FavoriteServices favoriteServices =new FavoriteServices();
			int i=favoriteServices.delete(favoriteId);
			if(i>0){
				result =new JsonResult(i, "删除收藏成功", Constants.STATUS_SUCCESS);
			}else{
				result =new JsonResult("", "删除收藏失败", Constants.STATUS_NOTFOUND);
			}
		} catch (Exception e) {
			result =new JsonResult(e.getMessage(), "删除收藏异常", Constants.STATUS_EX);
		}
		JsonResultWriter.writer(response, result);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
