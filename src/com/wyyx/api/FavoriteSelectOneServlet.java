package com.wyyx.api;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wyyx.model.Favorite;
import com.wyyx.services.FavoriteServices;
import com.wyyx.vo.FavoriteGoods;

import commons.Constants;
import commons.JsonResult;
import commons.JsonResultWriter;

/**
 * Servlet implementation class FavoriteSelectOneServlet
 */
@WebServlet("/FavoriteSelectOneServlet")
public class FavoriteSelectOneServlet extends HttpServlet {
	/**
	 * 查看单个商品的所有信息
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String favoriteUserId=request.getParameter("favoriteUserId");
		String favoriteGoodsId=request.getParameter("favoriteGoodsId");
		JsonResult result =null;
		try {
			FavoriteServices favoriteServices =new FavoriteServices();
			FavoriteGoods favoriteGoods =favoriteServices.selectone(favoriteUserId, favoriteGoodsId);
			if (favoriteGoods!=null) {
				result =new JsonResult(favoriteGoods, "查看成功", Constants.STATUS_SUCCESS);
			} else {
				result =new JsonResult("", "查看失败", Constants.STATUS_NOTFOUND);
			}
		} catch (Exception e) {
			result =new JsonResult(e.getMessage(), "查看异常", Constants.STATUS_EX);
		}
		JsonResultWriter.writer(response, result);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
