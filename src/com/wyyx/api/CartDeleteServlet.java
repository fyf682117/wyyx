package com.wyyx.api;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sun.corba.se.impl.orbutil.closure.Constant;
import com.wyyx.services.CartDeleteServices;

import commons.Constants;
import commons.JsonResult;
import commons.JsonResultWriter;



/**
 * Servlet implementation class CartDeleteServlet
 */
@WebServlet("/api/CartDeleteServlet")
public class CartDeleteServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String cartId =request.getParameter("cartId");
		JsonResult result =null;
		try {
			CartDeleteServices cartDeleteServices =new CartDeleteServices();
			int i =cartDeleteServices.cartDelete(cartId);
			System.out.println(i);
			if (i<=0) {
				result =new JsonResult(i, "移除成功", Constants.STATUS_SUCCESS);
			} else {
				result =new JsonResult("", "移除失败", Constants.STATUS_NOTFOUND);
			}
		} catch (Exception e) {
			result = new JsonResult(e.getMessage(), "移除异常", Constants.STATUS_EX);
		}
		JsonResultWriter.writer(response, result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
