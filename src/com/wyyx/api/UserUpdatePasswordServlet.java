package com.wyyx.api;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wyyx.services.UserUpdateServices;

import commons.Constants;
import commons.JsonResult;
import commons.JsonResultWriter;

/**
 * 更改密码
 */
@WebServlet("/api/UserUpdatePassword")
public class UserUpdatePasswordServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userId=request.getParameter("userId") ;
		String userPassword=request.getParameter("userPassword") ;
		JsonResult result =null;
		try {
			UserUpdateServices userUpdateServices = new UserUpdateServices();
			int num = userUpdateServices.updatePassword(userId, userPassword);
			if (num!=0) {
				result=new JsonResult("", "更改成功",Constants.STATUS_SUCCESS);
			} else {
				result=new JsonResult("", "请检查信息",Constants.STATUS_NOTFOUND);
			}
		} catch (Exception e) {
			result=new JsonResult(e.getMessage(), "更改失败", Constants.STATUS_EX);
		}
		JsonResultWriter.writer(response, result);
	}
}
