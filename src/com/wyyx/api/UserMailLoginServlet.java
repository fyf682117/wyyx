package com.wyyx.api;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wyyx.model.User;
import com.wyyx.services.UserLoginServices;

import commons.Constants;
import commons.JsonResult;
import commons.JsonResultWriter;

/**
 * 用户使用邮箱密码进行登录
 */
@WebServlet("/api/UserMailLogin")
public class UserMailLoginServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userEmail=request.getParameter("userEmail") ;
		String userPassword=request.getParameter("userPassword") ;
		JsonResult result =null;
		try {
			UserLoginServices userLoginServices =new UserLoginServices();
			User user=userLoginServices.userLogin(userEmail, userPassword);
			if (user!=null) {
				result=new JsonResult(user, "登录成功",Constants.STATUS_SUCCESS);
			} else {
				result=new JsonResult("", "登录账号错误",Constants.STATUS_NOTFOUND);
			}
		} catch (Exception e) {
			result=new JsonResult(e.getMessage(), "登录失败", Constants.STATUS_EX);
		}
		JsonResultWriter.writer(response, result);
	}
}
