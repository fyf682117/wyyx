package com.wyyx.api;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wyyx.services.CartAddServices;

import commons.Constants;
import commons.JsonResult;
import commons.JsonResultWriter;

/**
 * 添加购物车
 */
@WebServlet("/api/CartAdd")
public class CartAddServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String cartUserId=request.getParameter("cartUserId") ;
		String cartGoodsId=request.getParameter("cartGoodsId") ;
		String cartGoodsNumber=request.getParameter("cartGoodsNumber") ;
		JsonResult result =null;
		try {
			CartAddServices cartAddServices = new CartAddServices();
			int num = cartAddServices.cartAdd(cartUserId, cartGoodsId, cartGoodsNumber);
			if (num!=0) {
				result=new JsonResult("", "添加成功",Constants.STATUS_SUCCESS);
			} else {
				result=new JsonResult("", "添加失败",Constants.STATUS_NOTFOUND);
			}
		} catch (Exception e) {
			result=new JsonResult(e.getMessage(), "添加异常", Constants.STATUS_EX);
		}
		JsonResultWriter.writer(response, result);
	}
}
