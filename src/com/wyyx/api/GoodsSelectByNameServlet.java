package com.wyyx.api;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wyyx.model.Goods;
import com.wyyx.services.GoodsSelectServices;

import commons.Constants;
import commons.JsonResult;
import commons.JsonResultWriter;

/**
 * 通过搜索框模糊查询
 */
@WebServlet("/api/selectByName")
public class GoodsSelectByNameServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String goodsName = request.getParameter("goodsName");
		GoodsSelectServices goodsSelectServices = new GoodsSelectServices();
		List<Goods> list = goodsSelectServices.selectGoodsByName(goodsName);
		JsonResult result = null;
		
		try {
			if(list!=null) {
				result = new JsonResult(list,"查询成功",Constants.STATUS_SUCCESS);
			}else {
				result = new JsonResult<>("查询失败",Constants.STATUS_NOTFOUND);
			}
		}catch(Exception e) {
			result = new JsonResult<>(e.getMessage(),"查询异常",Constants.STATUS_EX);
		}
		JsonResultWriter.writer(response,result);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
