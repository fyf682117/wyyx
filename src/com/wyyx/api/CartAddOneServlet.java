package com.wyyx.api;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wyyx.services.CartUpdateServices;

import commons.Constants;
import commons.JsonResult;
import commons.JsonResultWriter;

/**
 * 商品数量加一
 */
@WebServlet("/api/CartAddOne")
public class CartAddOneServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String cartId=request.getParameter("cartId") ;
		JsonResult result =null;
		try {
			CartUpdateServices cartUpdateServices = new CartUpdateServices();
			int num = cartUpdateServices.cartAddOne(cartId);
			if (num!=0) {
				result=new JsonResult("", "加一成功",Constants.STATUS_SUCCESS);
			} else {
				result=new JsonResult("", "加一失败",Constants.STATUS_NOTFOUND);
			}
		} catch (Exception e) {
			result=new JsonResult(e.getMessage(), "加一异常", Constants.STATUS_EX);
		}
		JsonResultWriter.writer(response, result);
	}
}
