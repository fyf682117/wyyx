package com.wyyx.api;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wyyx.model.Goods;
import com.wyyx.services.UpdateGoodsServices;

import commons.Constants;
import commons.JsonResult;

/**
 * Servlet implementation class UpdateServlet
 */
@WebServlet("/UpdateServlet")
public class UpdateGoodsServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Goods goods = new Goods();
		goods.setGoodsStock(request.getParameter("goodStock"));
		goods.setGoodsPrice(request.getParameter("goodsPrice"));
		goods.setGoodsImage(request.getParameter("goodsImage"));
		goods.setGoodsStatus(request.getParameter("goodsComment"));
		UpdateGoodsServices updateGoodsServices = new UpdateGoodsServices();
		JsonResult result = null;
		int i = updateGoodsServices.updateGoods(goods);
		try {
			if(i>0) {
				result =new JsonResult<>("更新成功",Constants.STATUS_SUCCESS);
			}else {
				result =new JsonResult<>("更新失败",Constants.STATUS_SUCCESS);
			}
		}catch(Exception e) {
			result = new JsonResult<>("更新异常",Constants.STATUS_EX);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
