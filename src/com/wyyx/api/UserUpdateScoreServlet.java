package com.wyyx.api;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wyyx.services.UserUpdateServices;

import commons.Constants;
import commons.JsonResult;
import commons.JsonResultWriter;

/**
 * 更新用户积分
 */
@WebServlet("/api/UserUpdateScore")
public class UserUpdateScoreServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userId=request.getParameter("userId") ;
		String userScore=request.getParameter("userScore") ;
		JsonResult result =null;
		try {
			UserUpdateServices userUpdateServices = new UserUpdateServices();
			int num = userUpdateServices.updateScore(userId, userScore);
			if (num!=0) {
				result=new JsonResult("", "更新成功",Constants.STATUS_SUCCESS);
			} else {
				result=new JsonResult("", "请检查信息",Constants.STATUS_NOTFOUND);
			}
		} catch (Exception e) {
			result=new JsonResult(e.getMessage(), "更新失败", Constants.STATUS_EX);
		}
		JsonResultWriter.writer(response, result);
	}
}
