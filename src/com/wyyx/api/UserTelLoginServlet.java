package com.wyyx.api;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.wyyx.model.User;
import com.wyyx.services.UserLoginServices;

import commons.Constants;
import commons.JsonResult;
import commons.JsonResultWriter;

/**
 * 用户使用短信验证进行登录
 */
@WebServlet("/api/UserTelLogin")
public class UserTelLoginServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userTelphone=request.getParameter("userTelphone") ;
		String code = request.getParameter("code") ;
		HttpSession session = request.getSession();
	    String recode = session.getAttribute("code").toString();
		JsonResult result =null;
		User user = null;
		try {
			if(code.equals(recode)) {
				UserLoginServices userLoginServices =new UserLoginServices();
				user=userLoginServices.userLogin(userTelphone);
				if (user!=null) {
					result=new JsonResult(user, "登录成功",Constants.STATUS_SUCCESS);
				} else {
					result=new JsonResult("", "登录账号错误",Constants.STATUS_NOTFOUND);
				}
			}else {
				result=new JsonResult("", "短信验证码错误",Constants.STATUS_NOTFOUND);
			}
		} catch (Exception e) {
			result=new JsonResult(e.getMessage(), "登录失败", Constants.STATUS_EX);
		}
		JsonResultWriter.writer(response, result);
	}
}
