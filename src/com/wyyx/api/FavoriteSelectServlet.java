package com.wyyx.api;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.wyyx.model.Favorite;
import com.wyyx.services.FavoriteServices;
import com.wyyx.vo.FavoriteGoods;

import commons.Constants;
import commons.JsonResult;
import commons.JsonResultWriter;

/**
 * Servlet implementation class FavoriteSelectServlet
 */

/**
 * Servlet implementation class FavoriteServlet
 */

@WebServlet("/api/FavoriteSelectServlet")
public class FavoriteSelectServlet extends HttpServlet {
/**
 * 查看收藏夹所有商品
 */

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String favoriteUserId=request.getParameter("favoriteUserId");
		JsonResult result=null;
		
		try {
			FavoriteServices favoriteServices =new FavoriteServices();
			List<FavoriteGoods> favoriteList =favoriteServices.selectAll(favoriteUserId);//
			if(!favoriteList.isEmpty()){
				result=new JsonResult(favoriteList,"查看收藏夹成功",Constants.STATUS_SUCCESS);
			}else{
				result = new JsonResult("", "查看收藏夹失败", Constants.STATUS_NOTFOUND);
			}
		} catch (Exception e) {
			result =new JsonResult(e.getMessage(), "查看收藏夹异常", Constants.STATUS_EX);
		}
		JsonResultWriter.writer(response, result);
	}

	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}
