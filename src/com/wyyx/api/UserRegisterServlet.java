package com.wyyx.api;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.wyyx.model.User;
import com.wyyx.services.UserRegisterServices;

import commons.Constants;
import commons.JsonResult;
import commons.JsonResultWriter;

/**
 * 用户注册
 */
@WebServlet("/api/UserRegister")
public class UserRegisterServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userUserName=request.getParameter("userUserName") ;
		String userTelphone=request.getParameter("userTelphone") ;
		String userEmail=request.getParameter("userEmail") ;
		String userPassword=request.getParameter("userPassword") ;
		String code = request.getParameter("code") ;
		HttpSession session = request.getSession();
	    String recode = session.getAttribute("code").toString();
		JsonResult result =null;
		try {
			if(code.equals(recode)) {
				UserRegisterServices userRegisterServices = new UserRegisterServices();
				int num = userRegisterServices.register(userUserName, userTelphone, userEmail, userPassword);
				if (num!=0) {
					result=new JsonResult("", "注册成功",Constants.STATUS_SUCCESS);
				} else {
					result=new JsonResult("", "请检查信息",Constants.STATUS_NOTFOUND);
				}
			}else {
				result=new JsonResult("", "短信验证码错误",Constants.STATUS_NOTFOUND);
			}
		} catch (Exception e) {
			result=new JsonResult(e.getMessage(), "注册失败", Constants.STATUS_EX);
		}
		JsonResultWriter.writer(response, result);
	}
}
