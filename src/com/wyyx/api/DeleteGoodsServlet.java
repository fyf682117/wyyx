package com.wyyx.api;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wyyx.services.DeleteGoodsServices;

import commons.Constants;
import commons.JsonResult;
import commons.JsonResultWriter;

/**
 * 删除商品
 */
@WebServlet("/DeleteGoodsServlet")
public class DeleteGoodsServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String goodsId = request.getParameter("goodsId");
		DeleteGoodsServices deleteGoodsServlet = new DeleteGoodsServices();
		JsonResult result = null;
		int i = deleteGoodsServlet.deleteGoods(goodsId);
		try {
			if(i>0) {
				result = new JsonResult<>("删除成功",Constants.STATUS_SUCCESS);
			}else {
				result = new JsonResult<>("删除失败",Constants.STATUS_EX);
			}
		}catch(Exception e) {
			result = new JsonResult<>("删除异常",Constants.STATUS_EX);
		}
		JsonResultWriter.writer(response, result);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
