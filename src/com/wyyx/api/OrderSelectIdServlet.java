package com.wyyx.api;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wyyx.model.Order;
import com.wyyx.services.OrderSelectServices;

import commons.Constants;
import commons.JsonResult;
import commons.JsonResultWriter;

/**
 * 根据订单ID查询单个订单
 */
@WebServlet("/api/OrderSelectId")
public class OrderSelectIdServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String orderId=request.getParameter("orderId") ;
		JsonResult result =null;
		Order order = null;
		try {
			OrderSelectServices orderSelectServices =new OrderSelectServices();
			order=orderSelectServices.orderSelectId(orderId);
			if (order!=null) {
				result=new JsonResult(order, "查询成功",Constants.STATUS_SUCCESS);
			} else {
				result=new JsonResult("", "该订单不存在",Constants.STATUS_NOTFOUND);
			}
		} catch (Exception e) {
			result=new JsonResult(e.getMessage(), "查询失败", Constants.STATUS_EX);
		}
		JsonResultWriter.writer(response, result);
	}
}
