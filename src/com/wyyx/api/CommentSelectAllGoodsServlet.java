package com.wyyx.api;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wyyx.model.Comment;
import com.wyyx.services.CommentServices;

import commons.Constants;
import commons.JsonResult;
import commons.JsonResultWriter;

/**
 * Servlet implementation class CommentSelectAllGoodsServlet
 */
@WebServlet("/api/CommentSelectAllGoodsServlet")
public class CommentSelectAllGoodsServlet extends HttpServlet {
	/**
	 * 查看商品所有的评论
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String commentGoodsId =request.getParameter("comment_goods_id");
		
		JsonResult result =null;
		try {
			CommentServices commentServices =new CommentServices();
			List<Comment> commentList2 =commentServices.selectAll2(commentGoodsId);
			if (commentList2!=null) {
				result =new JsonResult(commentList2, "查看成功", Constants.STATUS_SUCCESS);
			} else {
				result =new JsonResult("", "查看失败", Constants.STATUS_NOTFOUND);
			}
		} catch (Exception e) {
			result =new JsonResult(e.getMessage(), "查看异常", Constants.STATUS_EX);
		}
		JsonResultWriter.writer(response, result);
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
