package com.wyyx.api;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wyyx.model.Comment;
import com.wyyx.services.CommentServices;

import commons.Constants;
import commons.JsonResult;
import commons.JsonResultWriter;

/**
 * Servlet implementation class CommentSelectAllUserServlet
 */
@WebServlet("/api/CommentSelectAllUserServlet")
public class CommentSelectAllUserServlet extends HttpServlet {
/**
 *  查看用户个人的所有评论
 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String commentUserId=request.getParameter("comment_user_id");
		JsonResult result =null;
		
		try {
			CommentServices  commentServices =new CommentServices();
			List<Comment> commentList=commentServices.selectAll(commentUserId);
			if (commentList!=null) {
				result =new JsonResult(commentList, "查看成功", Constants.STATUS_SUCCESS);
			} else {
				result =new JsonResult("", "查看失败", Constants.STATUS_NOTFOUND);
			}
		} catch (Exception e) {
			result =new JsonResult(e.getMessage(), "查看异常", Constants.STATUS_EX);
		}
		JsonResultWriter.writer(response, result);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
