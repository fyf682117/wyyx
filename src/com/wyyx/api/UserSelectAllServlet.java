package com.wyyx.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wyyx.model.User;
import com.wyyx.services.UserSelectServices;

import commons.Constants;
import commons.JsonResult;
import commons.JsonResultWriter;

/**
 * 查询所有用户
 */
@WebServlet("/api/UserSelectAll")
public class UserSelectAllServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JsonResult result =null;
		List<User> users = new ArrayList<User>();
		try {
			UserSelectServices userSelectServices =new UserSelectServices();
			users=userSelectServices.userSelect();
			if (!users.isEmpty()) {
				result=new JsonResult(users, "查询成功",Constants.STATUS_SUCCESS);
			} else {
				result=new JsonResult("", "查询失败",Constants.STATUS_NOTFOUND);
			}
		} catch (Exception e) {
			result=new JsonResult(e.getMessage(), "查询异常", Constants.STATUS_EX);
		}
		JsonResultWriter.writer(response, result);
	}
}
