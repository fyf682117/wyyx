package com.wyyx.api;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wyyx.services.UserUpdateServices;

import commons.Constants;
import commons.JsonResult;
import commons.JsonResultWriter;

/**
 * 用户实名认证
 */
@WebServlet("/api/UserUpdateAuthentication")
public class UserUpdateAuthenticationServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userId=request.getParameter("userId") ;
		String userRealName=request.getParameter("userRealName") ;
		String userIdNumber=request.getParameter("userIdNumber") ;
		JsonResult result =null;
		try {
			UserUpdateServices userUpdateServices = new UserUpdateServices();
			int num = userUpdateServices.updateAuthentication(userId, userRealName, userIdNumber);
			if (num!=0) {
				result=new JsonResult("", "认证成功",Constants.STATUS_SUCCESS);
			} else {
				result=new JsonResult("", "请检查信息",Constants.STATUS_NOTFOUND);
			}
		} catch (Exception e) {
			result=new JsonResult(e.getMessage(), "认证失败", Constants.STATUS_EX);
		}
		JsonResultWriter.writer(response, result);
	}
}
