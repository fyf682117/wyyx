package com.wyyx.api;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wyyx.model.Goods;
import com.wyyx.services.AddGoodsServices;

import commons.Constants;
import commons.JsonResult;
import commons.JsonResultWriter;

/**
 * Servlet implementation class AddGoodsServlet
 */
@WebServlet("/AddGoodsServlet")
public class AddGoodsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Goods goods = new Goods();
		goods.setGoodComment(request.getParameter("goodsComment"));
		goods.setGoodsAddress(request.getParameter("goodsAddres"));
		goods.setGoodsBuyNumber(request.getParameter("goodsBuyNumber"));
		goods.setGoodsCollectionNumber(request.getParameter("goodsCollectionNumber"));
		goods.setGoodsClickNumber(request.getParameter("goodsClickNumber"));
		goods.setGoodsColor(request.getParameter("goodsColor"));
		goods.setGoodsDiscount(request.getParameter("goodsDiscount"));
		goods.setGoodsImage(request.getParameter("goodsImage"));
		goods.setGoodsName(request.getParameter("goodsName"));
		goods.setGoodsPrice(request.getParameter("goodsPrice"));
		goods.setGoodsSize(request.getParameter("goodsSize"));
		goods.setGoodsSmallClassId(request.getParameter("goodsSmallClassId"));
		goods.setGoodsStatus(request.getParameter("GoodStatus"));
		goods.setGoodsUnit(request.getParameter("goodsUnit"));
		goods.setGoodsStock(request.getParameter("goodsStock"));
		AddGoodsServices addGoodsServices = new AddGoodsServices();
		JsonResult result =null;
		
		int i = addGoodsServices.addGoods(goods);
		try {
			if(i>0) {
					result = new JsonResult<>("添加成功",Constants.STATUS_SUCCESS);
			}else {
				result = new JsonResult<>("添加失败",Constants.STATUS_NOTFOUND);
			}
		}catch(Exception e) {
			result = new JsonResult<>("添加异常",Constants.STATUS_EX);
		}
		JsonResultWriter.writer(response,result);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
