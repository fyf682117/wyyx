package com.wyyx.api;

import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sun.corba.se.impl.orbutil.closure.Constant;

import commons.Config;
import commons.HttpUtil;
import commons.JsonResultWriter;
import commons.Constants;

/**
 * 获取验证码短信验证码接口
 */
@WebServlet("/api/SMSCheckServlet")
public class SMSCheckServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//生成6位验证码
		int code=(int)(Math.random()*(9999-1000+1))+100000;
		HttpSession session = request.getSession();
	    // 将认证码存入SESSION
	    session.setAttribute("code", code);
		String smsContent = "【永富科技】登录验证码："+code+"，如非本人操作，请忽略此短信。";
		String to = request.getParameter("telphone");
		String tmpSmsContent = null;
	    try{
	      tmpSmsContent = URLEncoder.encode(smsContent, "UTF-8");
	    }catch(Exception e){
	      
	    }
	    String url = Config.BASE_URL + Constants.OPERATION;
	    String body = "accountSid=" + Constants.ACCOUNTSID + "&to=" + to + "&smsContent=" + tmpSmsContent
	        + HttpUtil.createCommonParam();

	    // 提交请求
	    String result = HttpUtil.post(url, body);
	    System.out.println("result:" + System.lineSeparator() + result);

	}
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}
	
	
}
