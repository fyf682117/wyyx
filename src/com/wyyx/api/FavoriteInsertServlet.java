package com.wyyx.api;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wyyx.services.FavoriteServices;

import commons.Constants;
import commons.JsonResult;
import commons.JsonResultWriter;

/**
 * Servlet implementation class FavoriteInsertServlet
 */
@WebServlet("/api/FavoriteInsertServlet")
public class FavoriteInsertServlet extends HttpServlet {
	/**
	 * 收藏夹增加商品的
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String favoriteUserId =request.getParameter("favoriteUserId");
		String favoriteGoodsId =request.getParameter("favoriteGoodsId");
		JsonResult result =null;
		try {
			FavoriteServices favoriteServices =new FavoriteServices();
			int i=favoriteServices.insert(favoriteUserId, favoriteGoodsId);
			if (i>0) {
				result =new JsonResult(i, "添加收藏成功", Constants.STATUS_SUCCESS);
			} else {
				result =new JsonResult("", "添加收藏失败", Constants.STATUS_NOTFOUND);
			}
		} catch (Exception e) {
			result =new JsonResult(e.getMessage(), "添加收藏异常", Constants.STATUS_EX);
		}
		JsonResultWriter.writer(response, result);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		doGet(request, response);
	}

}
