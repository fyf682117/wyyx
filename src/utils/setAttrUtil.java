package utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.wyyx.model.Cart;
import com.wyyx.model.Comment;
import com.wyyx.model.Coupon;
import com.wyyx.model.Favorite;
import com.wyyx.model.Goods;
import com.wyyx.model.GoodsOrder;
import com.wyyx.model.LargeClass;
import com.wyyx.model.Order;
import com.wyyx.model.SmallClass;
import com.wyyx.model.User;
import com.wyyx.vo.CartGoods;
import com.wyyx.vo.FavoriteGoods;

public class setAttrUtil {
	/**
	 * 商品工具方法
	 * @param obj
	 * @param list
	 * @return
	 */
	public static  List<Goods> setGoods(List<HashMap<String,Object>> list){
			Goods goods = null;
			HashMap<String,Object> map = new HashMap<>();
			List<Goods> goodsList = new ArrayList<>();
			if(list!=null&&list.size()>0) {	
				for(int i = 0;i<list.size();i++) {
					map = list.get(i);
					goods = new Goods();
					goods.setGoodsId(StringUtils.valueOf(map.get("goods_id")));
					goods.setGoodsName(StringUtils.valueOf(map.get("goods_name")));
					goods.setGoodsPrice(StringUtils.valueOf(map.get("goods_price")));
					goods.setGoodsUnit(StringUtils.valueOf(map.get("goods_unit")));
					goods.setGoodsSize(StringUtils.valueOf(map.get("goods_size")));
					goods.setGoodsColor(StringUtils.valueOf(map.get("goods_color")));
					goods.setGoodsImage(StringUtils.valueOf(map.get("goods_image")));
					goods.setGoodsStock(StringUtils.valueOf(map.get("goods_stock")));
					goods.setGoodsAddress(StringUtils.valueOf(map.get("goods_address")));
					goods.setGoodsDiscount(StringUtils.valueOf(map.get("goods_discount")));
					goods.setGoodsAddTime(StringUtils.valueOf(map.get("goods_add_time")));
					goods.setGoodsClickNumber(StringUtils.valueOf(map.get("goods_click_number")));
					goods.setGoodsCollectionNumber(StringUtils.valueOf(map.get("goods_collection_number")));
					goods.setGoodsBuyNumber(StringUtils.valueOf(map.get("goods_buy_number")));
					goods.setGoodComment(StringUtils.valueOf(map.get("goods_comment")));
					goods.setGoodsSmallClassId(StringUtils.valueOf(map.get("goods_small_class_id")));
					goods.setGoodsStatus(StringUtils.valueOf(map.get("goods_status")));
					goodsList.add(goods);
				}
			}
		return  goodsList;
	}
	/**
	 * 用户工具方法
	 * @param obj
	 * @param list
	 * @return
	 */
		public static  List<User> setUser(List<HashMap<String,Object>> list){
			User user = null;
			HashMap<String,Object> map = new HashMap<>();
			List<User> userList = new ArrayList<>();
			if(list!=null&&list.size()>0) {	
				for(int i = 0;i<list.size();i++) {
					map = list.get(i);
					user = new User();
					user.setUserId(StringUtils.valueOf(map.get("user_id")));
					user.setUserAddress(StringUtils.valueOf(map.get("user_address")));
					user.setUserBirthday(StringUtils.valueOf(map.get("user_birthday")));
					user.setUserEmail(StringUtils.valueOf(map.get("user_email")));
					user.setUserIdNumber(StringUtils.valueOf(map.get("user_id_number")));
					user.setUserImage(StringUtils.valueOf(map.get("user_image")));
					user.setUserName(StringUtils.valueOf(map.get("user_name")));
					user.setUserPassword(StringUtils.valueOf(map.get("user_password")));
					user.setUserRealName(StringUtils.valueOf(map.get("user_real_name")));
					user.setUserRegisterTime(StringUtils.valueOf(map.get("user_register_tima")));
					user.setUserScore(StringUtils.valueOf(map.get("user_score")));
					user.setUserSex(StringUtils.valueOf(map.get("user_sex")));
					user.setUserStatus(StringUtils.valueOf(map.get("user_status")));
					user.setUserTelphone(StringUtils.valueOf(map.get("user_telphone")));
					userList.add(user);
				}
			}
		return  userList;
		}
		/**
		 * 订单工具方法
		 * @param obj
		 * @param list
		 * @return
		 */
		public static  List<Order> setOrder(List<HashMap<String,Object>> list){
			Order order = null;
			HashMap<String,Object> map = new HashMap<>();
			List<Order> orderList = new ArrayList<>();
			if(list!=null&&list.size()>0) {	
				for(int i = 0;i<list.size();i++) {
					map = list.get(i);
					order = new Order();
					order.setOrderId(StringUtils.valueOf(map.get("order_id")));
					order.setOrderAddress(StringUtils.valueOf(map.get("order_address")));
					order.setOrderAddTime(StringUtils.valueOf(map.get("order_add_time")));
					order.setOrderStatus(StringUtils.valueOf(map.get("order_status")));
					order.setOrderUserId(StringUtils.valueOf(map.get("order_user_id")));
					orderList.add(order);
				}
			}
		return  orderList;
		}
		/**
		 * 购物车工具方法
		 * @param obj
		 * @param list
		 * @return
		 */
		public static  List<Cart> setCart(List<HashMap<String,Object>> list){
			Cart cart = null;
			HashMap<String,Object> map = new HashMap<>();
			List<Cart> cartList = new ArrayList<>();
			if(list!=null&&list.size()>0) {	
				for(int i = 0;i<list.size();i++) {
					map = list.get(i);
					cart = new Cart();
					cart.setCartAddTime(StringUtils.valueOf(map.get("cart_add_time")));
					cart.setCartGoodsId(StringUtils.valueOf(map.get("cart_goods_id")));
					cart.setCartGoodsNumber(StringUtils.valueOf(map.get("cart_goods_number")));
					cart.setCartId(StringUtils.valueOf(map.get("cart_id")));
					cart.setCartStatus(StringUtils.valueOf(map.get("cart_status")));
					cart.setCartUserId(StringUtils.valueOf(map.get("cart_user_id")));
					cartList.add(cart);
				}
			}
		return  cartList;
		}
		/**
		 * 购物车商品工具方法
		 * 
		 * @param list
		 * @return
		 */
		public static  List<CartGoods> setCartGoods(List<HashMap<String,Object>> list){
			CartGoods cart = null;
			HashMap<String,Object> map = new HashMap<>();
			List<CartGoods> cartList = new ArrayList<>();
			if(list!=null&&list.size()>0) {	
				for(int i = 0;i<list.size();i++) {
					map = list.get(i);
					cart = new CartGoods();
					cart.setCartAddTime(StringUtils.valueOf(map.get("cart_add_time")));
					cart.setCartGoodsId(StringUtils.valueOf(map.get("cart_goods_id")));
					cart.setCartGoodsNumber(StringUtils.valueOf(map.get("cart_goods_number")));
					cart.setCartId(StringUtils.valueOf(map.get("cart_id")));
					cart.setCartStatus(StringUtils.valueOf(map.get("cart_status")));
					cart.setCartUserId(StringUtils.valueOf(map.get("cart_user_id")));
					Goods goods = new Goods();
					goods.setGoodsId(StringUtils.valueOf(map.get("goods_id")));
					goods.setGoodsName(StringUtils.valueOf(map.get("goods_name")));
					goods.setGoodsPrice(StringUtils.valueOf(map.get("goods_price")));
					goods.setGoodsUnit(StringUtils.valueOf(map.get("goods_unit")));
					goods.setGoodsSize(StringUtils.valueOf(map.get("goods_size")));
					goods.setGoodsColor(StringUtils.valueOf(map.get("goods_color")));
					goods.setGoodsImage(StringUtils.valueOf(map.get("goods_image")));
					goods.setGoodsStock(StringUtils.valueOf(map.get("goods_stock")));
					goods.setGoodsAddress(StringUtils.valueOf(map.get("goods_address")));
					goods.setGoodsDiscount(StringUtils.valueOf(map.get("goods_discount")));
					goods.setGoodsAddTime(StringUtils.valueOf(map.get("goods_add_time")));
					goods.setGoodsClickNumber(StringUtils.valueOf(map.get("goods_click_number")));
					goods.setGoodsCollectionNumber(StringUtils.valueOf(map.get("goods_collection_number")));
					goods.setGoodsBuyNumber(StringUtils.valueOf(map.get("goods_buy_number")));
					goods.setGoodComment(StringUtils.valueOf(map.get("goods_comment")));
					goods.setGoodsSmallClassId(StringUtils.valueOf(map.get("goods_small_class_id")));
					goods.setGoodsStatus(StringUtils.valueOf(map.get("goods_status")));
					cart.setGoods(goods);
					cartList.add(cart);
				}
			}
		return  cartList;
		}
		/**
		 * 优惠券工具方法
		 * @param obj
		 * @param list
		 * @return
		 */
		
		public static  List<Coupon> setCoupon(List<HashMap<String,Object>> list){
			Coupon coupon = null;
			HashMap<String,Object> map = new HashMap<>();
			List<Coupon> couponList = new ArrayList<>();
			if(list!=null&&list.size()>0) {	
				for(int i = 0;i<list.size();i++) {
					map = list.get(i);
					coupon = new Coupon();
					coupon.setCouponDiscountPrice(StringUtils.valueOf(map.get("coupon_discount_price")));
					coupon.setCouponeEndTime(StringUtils.valueOf(map.get("coupon_end_time")));
					coupon.setCouponId(StringUtils.valueOf(map.get("coupon_id")));
					coupon.setCouponLimitPrice(StringUtils.valueOf(map.get("coupon_limit_price")));
					coupon.setCouponName(StringUtils.valueOf(map.get("coupon_name")));
					coupon.setCouponOrderId(StringUtils.valueOf(map.get("coupon_order_id")));
					coupon.setCouponStartTime(StringUtils.valueOf(map.get("coupon_start_time")));
					coupon.setCouponStatus(StringUtils.valueOf(map.get("coupon_discount_status")));
					coupon.setCouponUserId(StringUtils.valueOf(map.get("coupon_user_id")));
					couponList.add(coupon);
					
				}
			}
		return  couponList;
		}
		/**
		 * 收藏工具方法
		 * @param obj
		 * @param list
		 * @return
		 */
		public static  List<Favorite> setFavorite(List<HashMap<String,Object>> list){
			Favorite favorite = null;
			HashMap<String,Object> map = new HashMap<>();
			List<Favorite> favoriteList = new ArrayList<>();
			if(list!=null&&list.size()>0) {	
				for(int i = 0;i<list.size();i++) {
					map = list.get(i);
					favorite = new Favorite();
					favorite.setFavoriteGoodsId(StringUtils.valueOf(map.get(map.get("favorite_goods_id"))));
					favorite.setFavoriteAddTime(StringUtils.valueOf(map.get("favorite_add_time")));
					favorite.setFavoriteId(StringUtils.valueOf(map.get("favorite_id")));
					favorite.setFavoriteStatus(StringUtils.valueOf(map.get("favorite_status")));
					favorite.setFavoriteUserId(StringUtils.valueOf(map.get("favorite_user_id")));
					favoriteList.add(favorite);
				}
			}
		return  favoriteList;
		}
		
		/**
		 * 收藏夹商品工具方法
		 * 
		 * @param list
		 * @return
		 */
		public static  List<FavoriteGoods> setFavoriteGoods(List<HashMap<String,Object>> list){
			FavoriteGoods favorite = null;
			HashMap<String,Object> map = new HashMap<>();
			List<FavoriteGoods> favoriteList = new ArrayList<>();
			if(list!=null&&list.size()>0) {	
				for(int i = 0;i<list.size();i++) {
					map = list.get(i);
					favorite = new FavoriteGoods();
					favorite.setFavoriteGoodsId(StringUtils.valueOf(map.get(map.get("favorite_goods_id"))));
					favorite.setFavoriteAddTime(StringUtils.valueOf(map.get("favorite_add_time")));
					favorite.setFavoriteId(StringUtils.valueOf(map.get("favorite_id")));
					favorite.setFavoriteStatus(StringUtils.valueOf(map.get("favorite_status")));
					favorite.setFavoriteUserId(StringUtils.valueOf(map.get("favorite_user_id")));
					Goods goods = new Goods();
					goods.setGoodsId(StringUtils.valueOf(map.get("goods_id")));
					goods.setGoodsName(StringUtils.valueOf(map.get("goods_name")));
					goods.setGoodsPrice(StringUtils.valueOf(map.get("goods_price")));
					goods.setGoodsUnit(StringUtils.valueOf(map.get("goods_unit")));
					goods.setGoodsSize(StringUtils.valueOf(map.get("goods_size")));
					goods.setGoodsColor(StringUtils.valueOf(map.get("goods_color")));
					goods.setGoodsImage(StringUtils.valueOf(map.get("goods_image")));
					goods.setGoodsStock(StringUtils.valueOf(map.get("goods_stock")));
					goods.setGoodsAddress(StringUtils.valueOf(map.get("goods_address")));
					goods.setGoodsDiscount(StringUtils.valueOf(map.get("goods_discount")));
					goods.setGoodsAddTime(StringUtils.valueOf(map.get("goods_add_time")));
					goods.setGoodsClickNumber(StringUtils.valueOf(map.get("goods_click_number")));
					goods.setGoodsCollectionNumber(StringUtils.valueOf(map.get("goods_collection_number")));
					goods.setGoodsBuyNumber(StringUtils.valueOf(map.get("goods_buy_number")));
					goods.setGoodComment(StringUtils.valueOf(map.get("goods_comment")));
					goods.setGoodsSmallClassId(StringUtils.valueOf(map.get("goods_small_class_id")));
					goods.setGoodsStatus(StringUtils.valueOf(map.get("goods_status")));
					favorite.setGoods(goods);
					favoriteList.add(favorite);
				}
			}
		return  favoriteList;
		}
		
		/**
		 * goodOrder工具方法
		 * @param obj
		 * @param list
		 * @return
		 */
		public static  List<GoodsOrder> setGoodsOrder(List<HashMap<String,Object>> list){
			GoodsOrder goodsOrder = null;
			HashMap<String,Object> map = new HashMap<>();
			List<GoodsOrder> goodOrderList = new ArrayList<>();
			if(list!=null&&list.size()>0) {	
				for(int i = 0;i<list.size();i++) {
					map = list.get(i);
					goodsOrder = new GoodsOrder();
					goodsOrder.setGoGoodsCount(StringUtils.valueOf(map.get("go_goods_count")));
					goodsOrder.setGoGoodsId(StringUtils.valueOf(map.get("go_goods_id")));
					goodsOrder.setGoId(StringUtils.valueOf(map.get("go_id")));
					goodsOrder.setGoOrderId(StringUtils.valueOf(map.get("go_order_id")));
					goodOrderList.add(goodsOrder);
					
				}
			}
		return  goodOrderList;
		}
		/**
		 * 大类别工具方法
		 * @param obj
		 * @param list
		 * @return
		 */
		public static  List<LargeClass> setLargeClass(List<HashMap<String,Object>> list){
			LargeClass largeClass = null;
			HashMap<String,Object> map = new HashMap<>();
			List<LargeClass> largeClassList = new ArrayList<>();
			if(list!=null&&list.size()>0) {	
				for(int i = 0;i<list.size();i++) {
					map = list.get(i); 
					largeClass = new LargeClass();
					largeClass.setLargeClassId(StringUtils.valueOf(map.get("large_class_id")));
					largeClass.setLargeClassName(StringUtils.valueOf(map.get("large_class_name")));
					largeClass.setLargeClassStatus(StringUtils.valueOf(map.get("large_class_status")));
					largeClass.setLargeClassTime(StringUtils.valueOf(map.get("large_class_time")));
					largeClassList.add(largeClass);
					
				}
			}
		return  largeClassList;
		}
		/**
		 * 小类别工具方法
		 * @param obj
		 * @param list
		 * @return
		 */
		public static List<SmallClass> setSmallClass(List<HashMap<String,Object>> list){
			SmallClass smallClass =null;
			HashMap<String,Object> map = new HashMap<>();
			List<SmallClass> smallClassList = new ArrayList<>();
			if(list!=null&&list.size()>0) {
				for(int i = 0;i<list.size();i++) {
					map = list.get(i);
					smallClass = new SmallClass();
					smallClass.setSmallClassId(StringUtils.valueOf(map.get("small_class_id")));
					smallClass.setSmallClassLargeClassId(StringUtils.valueOf(map.get("small_class_large_class_id")));
					smallClass.setSmallClassName(StringUtils.valueOf(map.get("small_class_name")));
					smallClass.setSmallClassStatus(StringUtils.valueOf(map.get("small_class_status")));
					smallClass.setSmallClassTime(StringUtils.valueOf(map.get("small_class_time")));
					smallClassList.add(smallClass);
				}
			}
			return smallClassList;
		}
		/**
		 * 评论查询
		 * @param obj
		 * @param list
		 * @return
		 */
		public static List<Comment> setComment(List<HashMap<String,Object>> list){
			Comment comment =null;
			HashMap<String,Object> map = new HashMap<>();
			List<Comment> commentList = new ArrayList<>();
			if(list!=null&&list.size()>0) {
				for(int i = 0;i<list.size();i++) {
					map = list.get(i);
					comment = new Comment();
					comment.setCommentId(StringUtils.valueOf(map.get("comment_id")));
					comment.setCommentGoodsId(StringUtils.valueOf(map.get("comment_goods_id")));
					comment.setCommentUserId(StringUtils.valueOf(map.get("comment_user_id")));
					comment.setCommentStatus(StringUtils.valueOf(map.get("comment_status")));
					comment.setCommentContent(StringUtils.valueOf(map.get("comment_content")));
					commentList.add(comment);
				}
			}
			 return commentList;
		}
}
