package utils;

import java.sql.ResultSet;
import java.util.List;

public interface RowHandlerMappers {
	<T>List<T> mapping(ResultSet rs);
}
